/**
 * Executes the routing algorithm
 */
private void routingAlg(){
	
	flowCount = 1;     // processed flows counter
	String msg = "";   // auxiliary String used to build messages 
	                   //before sending them		
	
	//---------------- While we still have pending flows ----------------//	
	while (!ses.isFullyRouted()) {
		Flow candidateFlow = null;
		boolean candidateFound = false;
		Flow flow = null;
		
		if (greenAlg){
			//------------- Search for candidate flow -------------------//
			for (Iterator<Flow> iterator = ses.getFlows().iterator(); iterator.hasNext(); ){
				
				flow = iterator.next();                           // get flow

				// if flow is already routed we skip:
				if (flow.getStatus() != Flow.RoutingStatus.PENDING) continue; 
				
				// if not pre-used, check if source and target nodes have enough resources
				if (!checkFlowEnds(flow, false) ) continue;
				
				// load active nodes capable to carry the flow and their edges into graph:
				loadCapableSubGraph(flow, activeNodes);
				
				// if there is a path between source and target in graph we have found a candidate flow:
				if (isConnected(flow)) {
					candidateFlow = flow;
					candidateFound = true;
					break;
				}
			}
		}
		
		//------- If no candidate flow, we try with a "random" flow -------//
		if (!candidateFound){ 
			candidateFlow = getRandomFlow();			
		}
		
		//-------------------- Process candidate flow -------------------//
		// display which flow we are processing:
		msg = displayProcessingFlow(flowCount, candidateFlow); 
		flowCount++;                  // increase current flow counter
		
		if (!candidateFound) {
			// check if source and target nodes have enough resources:
			if (!checkFlowEnds(candidateFlow, true) ) continue;
		
			// load all nodes capable to carry the flow and their edges into graph:
			loadCapableSubGraph(candidateFlow, null);
		}
	
		if (greenAlg && !singleResource)     // calculate node and edge weights
			calculateWeights(candidateFlow); 
		
		boolean routeFound = findRoute(candidateFlow);   // find shortest-path route	
		displayProgress(candidateFlow, msg, routeFound); // display progress
	}
	displayOnEnd();
}