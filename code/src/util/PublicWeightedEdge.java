package util;

import org.jgrapht.graph.DefaultWeightedEdge;

public class PublicWeightedEdge extends DefaultWeightedEdge{

	
	private static final long serialVersionUID = 1L;
	private int sourcePort;
	private int targetPort;

	@Override
	public Object getSource(){
		return super.getSource();
	}
	
	@Override
	public Object getTarget(){
		return super.getTarget();
	}
	
	@Override
	public double getWeight(){
		return super.getWeight();
	}
	
	/** @return port number where the connection is made in the source Node */
	public int getSourcePort() {return sourcePort;}
	
	/** @return port number where the connection is made in the target Node */
	public int getTargetPort() {return targetPort;}
	
	/**
	 * Stores the port number in which the connection is made in the source Node
	 * @param sourcePort port number where the connection is made in the source Node
	 */
	public void setSourcePort(int sourcePort){
		this.sourcePort = sourcePort;
	}
	
	/**
	 * Stores the port number in which the connection is made in the target Node
	 * @param targetPort port number where the connection is made in the target Node
	 */
	public void setTargetPort(int targetPort){
		this.targetPort = targetPort;
	}
	
	/**
	 * Stores the port numbers in which the connection is made in both source and target Nodes
	 * @param sourcePort port number where the connection is made in the source Node
	 * @param targetPort port number where the connection is made in the target Node
	 */
	public void setPorts(int sourcePort, int targetPort){
		this.sourcePort = sourcePort;
		this.targetPort = targetPort;
	}
	
}
