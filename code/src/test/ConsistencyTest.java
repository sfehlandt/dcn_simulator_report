package test;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Set;

import network.Flow;
import network.Session;
import sim.MRGRouting;
import sim.Messenger;
import sim.PrintStreamMessenger;
import sim.RoutingAlgorithm;
import sim.Simulator;

/**
 * Class used to test two sequential executions of simulations and compare their results.
 * May be used to run the exact same simulation and compare if the results are equal
 * @author sfehland
 */
public class ConsistencyTest {
	
	/**
	 * Main method, executes the test
	 * @param args array of arguments, currently unused
	 * @throws IOException
	 */
	public static void main (String[] args) throws IOException{
		
		int alg1  = MRGRouting.MR_GREEN;
		int alg2  = MRGRouting.MR_GREEN_SAVE_RESOURCES; 
		
		boolean printAbortedFlows = false; // true to print aborted flows lists
		boolean buildNet          = true; // false to build Network, true to load Session File
		boolean firstSimOnly      = false;
		
		int verbosity = RoutingAlgorithm.VERBOSITY_ONE_LINE;
		//int verbosity = RoutingAlgorithm.VERBOSITY_FLOW_PATH;
		//int verbosity = RoutingAlgorithm.VERBOSITY_DETAILED_PROGRESS;
		
		float mean   = (float) 0.02;
		float stdDev = (float) 0.02;
		
		int numFlows = 300;
		
		Messenger msn = new PrintStreamMessenger();
		Simulator simulator = new Simulator(msn);
		simulator.setRoutingAlgorithm(new MRGRouting());
		
		
		//---------------------------- Execution 1 --------------------------
		if (buildNet) {
			simulator.buildNetwork("bcubeServers_5_2");
			simulator.generateStaticTraffic(numFlows, mean, stdDev);
			simulator.saveSession("ConsistencySessionIn1");
		} else {
			simulator.loadSession("ConsistencySessionIn1");
		}
		
		Instant start = Instant.now();
		simulator.runSimulation(alg1, verbosity);
		Instant end = Instant.now();
		Duration duration1 = Duration.between(start, end);
		System.out.println("Duration: "+duration1 + ", in ms = " + duration1.toMillis());
		
		Session ses                  = simulator.getSession();
		Set<Flow> abortedFlows1      = ses.getAbortedFlows();
	
		simulator.saveSession("ConsistencySessionOut1");
		
		if (firstSimOnly) return;
		
		//---------------------------- Execution 2 --------------------------
		//simulator.loadSession("ConsistencySessionIn1");
		ses                      = simulator.getSession();
		//ses.setNumResources(1);
		simulator.resetSimulation();
		simulator.saveSession("ConsistencySessionIn2");
		
		simulator.runSimulation(alg2, verbosity);
		
		ses                           = simulator.getSession();
		Set<Flow> abortedFlows2       = ses.getAbortedFlows();
		
		
		//---------------------------- Results  --------------------------
		boolean set1in2 = abortedFlows2.containsAll(abortedFlows1);
		boolean set2in1 = abortedFlows1.containsAll(abortedFlows2);

		
		if (printAbortedFlows) {   // print aborted flows
			msn.sendMessage("abortedFlows1:");
			
			for (Flow flow : abortedFlows1)
				msn.sendMessage(flow.toString());
		
			msn.breakLine();
			msn.sendMessage("abortedFlows2:");
			
			for (Flow flow : abortedFlows2)
				msn.sendMessage(flow.toString());
		}
		
		// Compare sets of aborted flows:
		msn.sendMessage("Flows_1 subset of Flows_2 = " + set1in2);
		msn.sendMessage("Flows_2 subset of Flows_1 = " + set2in1);
		msn.sendMessage("Flows_1 = Flows_2? " + abortedFlows1.equals(abortedFlows2));
		
		simulator.saveSession("ConsistencySessionOut2");
	}
}
