package test;

import util.Array2D;
import util.Array3D;

public class ArrayTester {
	
	public static void testArray2D(){
		Array2D<String> array = new Array2D<String>(3,4);
		
		array.set(0,0,"x0,y0");
		array.set(0,1,"x0,y1");
		array.set(0,2,"x0,y2");
		array.set(0,3,"x0,y3");

		array.set(1,0,"x1,y0");
		array.set(1,1,"x1,y1");
		array.set(1,2,"x1,y2");
		array.set(1,3,"x1,y3");

		array.set(2,0,"x2,y0");
		array.set(2,1,"x2,y1");
		array.set(2,2,"x2,y2");
		array.set(2,3,"x2,y3");
		
		for (int i = 0; i < array.sizeX; i++){
			for (int j = 0; j < array.sizeY; j++)
				System.out.print(array.get(i, j) + " ");
			System.out.println("");
		}
	}
	
	public static void testArray3D(){
		Array3D<String> array = new Array3D<String>(2,3,4);
		
		array.set(0,0,0, "x0,y0,z0");
		array.set(0,0,1, "x0,y0,z1");
		array.set(0,0,2, "x0,y0,z2");
		array.set(0,0,3, "x0,y0,z3");

		array.set(0,1,0, "x0,y1,z0");
		array.set(0,1,1, "x0,y1,z1");
		array.set(0,1,2, "x0,y1,z2");
		array.set(0,1,3, "x0,y1,z3");

		array.set(0,2,0, "x0,y2,z0");
		array.set(0,2,1, "x0,y2,z1");
		array.set(0,2,2, "x0,y2,z2");
		array.set(0,2,3, "x0,y2,z3");


		array.set(1,0,0, "x1,y0,z0");
		array.set(1,0,1, "x1,y0,z1");
		array.set(1,0,2, "x1,y0,z2");
		array.set(1,0,3, "x1,y0,z3");

		array.set(1,1,0, "x1,y1,z0");
		array.set(1,1,1, "x1,y1,z1");
		array.set(1,1,2, "x1,y1,z2");
		array.set(1,1,3, "x1,y1,z3");

		array.set(1,2,0, "x1,y2,z0");
		array.set(1,2,1, "x1,y2,z1");
		array.set(1,2,2, "x1,y2,z2");
		array.set(1,2,3, "x1,y2,z3");

		
		for (int i = 0; i < array.sizeX; i++){
			for (int j = 0; j < array.sizeY; j++){
				for (int k = 0; k < array.sizeZ; k++)
					System.out.print(array.get(i, j, k) + " ");
				System.out.println("");
			}
			System.out.println("-----------------------------------------");
		}
	}
	
	public static void main (String[] args){
		System.out.println("Array2D:");	  testArray2D();
		System.out.println("\nArray3D:"); testArray3D();
	}
}
