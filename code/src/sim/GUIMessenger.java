package sim;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

public class GUIMessenger extends JFrame implements Messenger {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Message labels and line tabulation for messages:
	public static final String ERROR_LABEL       = "ERROR! ";
	public static final String FATAL_ERROR_LABEL = "FATAL ERROR! ";
	public static final String WARNING_LABEL     = "WARNING! ";
	public static final String lineTab           = "- ";
	private boolean sameLine = false;
	
	private JTextArea area;
	private int width  = 480;
	private int height = 360;
	
	public GUIMessenger(){
		super("Console");

		area = new JTextArea(width, height);
		
		JScrollPane scrollPane = new JScrollPane(area);
		DefaultCaret caret = (DefaultCaret)area.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		this.setContentPane(scrollPane);
		this.setSize(width, height);
		this.setVisible(true);
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) { 
				System.exit(0);
			}
		});
	}

	@Override
	public void breakLine(){
		area.append("\n");
		sameLine = false;
	}

	@Override
	public void sendError(String msg) {
		sameLine = false;
		msg = ERROR_LABEL + msg;
		sendMessage(msg);
	}

	@Override
	public void sendFatalError(String msg) {
		sameLine = false;
		msg = FATAL_ERROR_LABEL + msg;
		sendMessage(msg);
	}
		
	@Override
	public void sendMessage(String msg) {
		if (sameLine){
			area.append(msg + "\n");
			sameLine = false;
		} else
			area.append(lineTab + msg + "\n");
	}

	@Override
	public void sendWarning(String msg) {
		sameLine = false;
		msg = WARNING_LABEL + msg;
		sendMessage(msg);		
	}
	
	public static void main (String[] args){
		Messenger msn = new GUIMessenger();
		for (int i = 0; i<200; i++)
			msn.sendMessage("hola " + i);
	}

}
