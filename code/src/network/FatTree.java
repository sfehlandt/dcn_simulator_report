package network;

import java.io.PrintStream;
import java.util.Collection;

import util.Array2D;
import util.Array3D;

/**
 * Implementation of a FatTree network implementing the {@link Network} interface
 * 
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class FatTree implements Network {
	
	//******************************** ATTRIBUTES **********************************
	protected int numPods     = 0;           // number of pods
	protected int halfPods    = 0;           // half the number of pods
	protected int numLinks    = 0;           // number of links
	protected int numServers  = 0;           // number of servers
	protected int numSwitches = 0;           // number of switches
	protected Array2D<Node> coreSwitches;    // set of core switches
	protected Array2D<Node> podSwitches;     // set of pod switches (aggregation and edge)
	protected Array3D<Node> servers;         // set of servers
	protected int prefix;                    // network address prefix
	protected ServerParameters serverParams; // parameters for servers
	protected SwitchParameters switchParams; // parameters for switches
	protected boolean printFull = true;      // decide if print full nodes or just address
	protected int nodesCounter    = 0;       // counter to generate unique nodeIDs   or indexes
	protected int serversCounter  = 0;       // counter to generate unique serverIDs or indexes
	protected int switchesCounter = 0;       // counter to generate unique switchIDs or indexes
	protected Collection<Node> nodes = null; // unused collection of nodes, only to show association in UML Lab
	
	//****************************** CONSTRUCTORS **********************************
	protected FatTree(){}
	
	/**
     * Constructs a new FatTree network
     * @param netArgs    array of parameters to construct the network,  parsed as Strings.
     * @param serverArgs array of parameters to construct the servers,  parsed as Strings.
     * @param switchArgs array of parameters to construct the switches, parsed as Strings.
     */
	public FatTree(final String[] netArgs, final String[] serverArgs, final String[] switchArgs) {
		numPods            = Integer.parseInt(netArgs[0]);     // netArgs[0]: number of pods
   		prefix             = Integer.parseInt(netArgs[1]);     // netArgs[1]: network prefix
   		serverParams       = new ServerParameters(serverArgs); // serverArgs: server parameters
   		switchParams       = new SwitchParameters(switchArgs); // switchArgs: switch parameters
   		   		
   		if (numPods%2 != 0)	numPods++; // Ensure number of pods is even
   		
   		halfPods = numPods/2; 	    // half the number of pods (numPods/2), used for:
   									// - number of edge/aggregate switches in a pod
   									// - number of servers in an edge
   									// - dimension of the core-switches 2D-grid (halfPods x halfPods)
   		
   		numServers  = (int) numPods*numPods*numPods/4;
   		numSwitches = (int) halfPods*halfPods + numPods*numPods;
   		coreSwitches   = new Array2D<Node>(halfPods,halfPods);
   		podSwitches    = new Array2D<Node>(numPods,numPods);
   		servers        = new Array3D<Node>(numPods,halfPods,halfPods);
   		
   		createSwitchingDevices();
   		
   		//---- Connect edge and aggregate switches ----//
   		for (int pod = 0; pod < numPods; pod++){                 // for each pod		
   			for (int edge = 0; edge < halfPods; edge++){         // for each edge-switch
   				for (int agg = halfPods; agg < numPods; agg++){  // for each aggregate-switch
   					
   					podSwitches.get(pod,edge).connect(agg, edge, podSwitches.get(pod,agg));  // edge -> agg
   					podSwitches.get(pod,agg).connect(edge,  agg, podSwitches.get(pod,edge)); // agg  -> edge
   					numLinks++;
   				}
   			}
   		}
   		
   		//---- Create Servers and Connect to Pod Switches ----//		
   		for (int pod = 0; pod < numPods; pod++){              // for each pod		
   			for (int sw = 0; sw < halfPods; sw++){            // for each edge-switch				
   				for (int sv = 2; sv <= halfPods + 1; sv++){   // for each server
   					
   					// Create server:
   					servers.set(pod,sw,sv-2, 
   							new Server(serversCounter++, nodesCounter++, 1, 4, serverParams, 
   									new Address(prefix,pod,sw,sv)));
   					
   					// Connect server to edge switch:
   					servers.get(pod,sw,sv-2).connect(0, sv-2, podSwitches.get(pod,sw));    // server -> edge
   					podSwitches.get(pod,sw).connect(sv-2, 0,  servers.get(pod,sw,sv-2)); //   edge -> server
   					numLinks++;
   				}
   			}
   		}
   		
   		//---- Connect core and aggregate switches ----//
   		for (int i = 0; i < halfPods; i++){               // for first dimension (top-down)			
   			for (int j = 0; j < halfPods; j++){           // for second dimension (left-right)
   				for (int pod = 0; pod < numPods; pod++){  // for each pod
   					
   					coreSwitches.get(i,j).connect(pod, halfPods+j, podSwitches.get(pod,halfPods + i));// core -> agg
   					podSwitches.get(pod,halfPods + i).connect(halfPods+j, pod, coreSwitches.get(i,j));// agg  -> core
   					numLinks++;
   				}		
   			}
   		}
   	}
	
	//******************************** METHODS *************************************
	/**
	 * Builds the network and connects its components
	 */
	protected void createSwitchingDevices(){
   		
   		//---- Create Core Switches ----//
   		for (int o3 = 1; o3 <= halfPods; o3++) {     // for first dimension (top-down)			
   			for (int o4 = 1; o4 <= halfPods; o4++) { // for second dimension (left-right)
   				coreSwitches.set(o3-1,o4-1, 
   					new Switch(switchesCounter++, nodesCounter++, numPods, 3, switchParams, 
   							new Address(prefix,numPods,o3,o4)));
   			}
   		}   
   		//---- Create Pod Switches ----//
   		for (int pod = 0; pod < numPods; pod++) {    // for each pod			
   			for (int sw = 0; sw < numPods; sw++) {   // for each switch
   				podSwitches.set(pod,sw, 
   					new Switch(switchesCounter++, nodesCounter++, numPods, (pod < halfPods ? 1 : 2), switchParams, 
   							new Address(prefix,pod,sw,1)));
   			}
   		}
	}

	//******************************** METHODS *************************************
	@Override
	public int getNumLevels() {	return 3;}
	
	@Override
	public int getNumLinks()  {return this.numLinks;}
	
	@Override
	public int getNumNodes() {return (getNumServers() + getNumSwitches());}
	
	@Override
	public int getNumNodesON() {
		int counter = 0;
		
		//---- Check Core Switches ----//
   		for (int o3 = 1; o3 <= halfPods; o3++) {      // for first dimension (top-down)			
   			for (int o4 = 1; o4 <= halfPods; o4++) {  // for second dimension (left-right)
   				if (coreSwitches.get(o3-1,o4-1).isOn()) counter++;
   			}
   		}   
   		//---- Check Pod Switches ----//
   		for (int pod = 0; pod < numPods; pod++) {    // for each pod			
   			for (int sw = 0; sw < numPods; sw++) {   // for each switch
   				if (podSwitches.get(pod,sw).isOn())	counter++;
   			}
   		}
   		
   		//---- Check Servers ----//		
   		for (int pod = 0; pod < numPods; pod++) {            // for each pod		
   			for (int sw = 0; sw < halfPods; sw++) {          // for each edge-switch				
   				for (int sv = 2; sv <= halfPods + 1; sv++) { // for each server
   					if (servers.get(pod,sw,sv-2).isOn()) counter++;
   				}
   			}
   		}
		return counter;
	}
	
	@Override
	public int getNumResources(){ return serverParams.getNumResources();}
	
	@Override
	public int getNumServers() {return numServers;}
	
	@Override
	public int getNumSwitches() {return numSwitches;}
		
	@Override
	public Node getNode(final Address address) {
		int o2 = address.getOctet2();
		int o3 = address.getOctet3();
		int o4 = address.getOctet4();
		
		if (o2 == numPods ) return coreSwitches.get(o3-1,o4-1);	// it is a core-switch
		else if (o4 == 1)   return podSwitches.get(o2,o3);      // it is a pod-switch
		else                return servers.get(o2,o3,o4-2);     // it is a server
	}
	
	@Override
	public Node getNode(int nodeID) {
		int numSwitches = getNumSwitches();
		int numServers  = getNumServers();
		int numNodes = numSwitches + numServers;
		Node node = null;
		
		if (nodeID < numSwitches){
			node = getSwitch(nodeID);
			
		}else if(nodeID < numNodes){ // The rest are servers
			node = getServer(nodeID - numSwitches);
		
		}else{
			throw new IndexOutOfBoundsException("nodeID " + nodeID + " is out of bounds, maxNodeID = " + (numNodes-1));
		}
		
		if (node.getNodeID() == nodeID)
			return node;
		else
			return null;
	}
	
	@Override
	public Node getServer(int serverID) {
		Server server;
		int numServers = getNumServers();
		if(serverID < numServers){
			int sizeSw = halfPods*halfPods;
			int pod = serverID / sizeSw;
			int sw  = (serverID - pod*sizeSw) / halfPods;
			int sv  = serverID - pod*sizeSw - sw*halfPods;
			server = (Server) servers.get(pod, sw, sv);
		}else{
			throw new 
			IndexOutOfBoundsException("serverID " + serverID + " is out of bounds, maxServerID = " + (serverID-1));
		}
		
		if (server.getDeviceID() == serverID)
			return server;
		else
			throw new 
			RuntimeException("Error searching serverID " + serverID + ", server found has ID = "+server.getDeviceID());
	}
	
	@Override
	public Node getSwitch(int switchID) {
		int numCoreSws    = halfPods*halfPods; // First we have halfPods^2 coreSwitches
		int numPodSws     = numPods*numPods;   // Then  we have numPods^2  podSwitches
		int numSwitches   = numCoreSws + numPodSws;
		
		Node swt = null;
		
		if (switchID < numCoreSws){	                // The first halfPods^2 node are coreSwitches
			int x = switchID / halfPods;
			int y = switchID % halfPods;			
			swt = coreSwitches.get(x, y);
			
		}else if (switchID < numSwitches){ // Then  we have numPods^2  podSwitches
			int index = switchID - numCoreSws;
			int pod = index / numPods;
			int sw  = index % numPods;			
			swt = podSwitches.get(pod, sw);
			
		}else{
			throw new 
			IndexOutOfBoundsException("switchID " + switchID + " is out of bounds, maxSwitchID = " + (numSwitches-1));
		}
		
		if (swt.getDeviceID() == switchID)
			return swt;
		else
			throw new 
			RuntimeException("Error searching switchID " + switchID + ", switch found has ID = " + swt.getDeviceID());
			
	}
	
	@Override
	public String getServerLabel(boolean plural) {
		if (plural)
			return "end-hosts";
		else
			return "end-host";
	}
	
	@Override
	public String getSwitchLabel(boolean plural) {
		if (plural)
			return "switches";
		else
			return "switch";
	}
	
	@Override
	public double powerConsumption() {
		double power = 0;
		
		//---- Check Core Switches ----//
   		for (int o3 = 1; o3 <= halfPods; o3++) {     // for first dimension (top-down)			
   			for (int o4 = 1; o4 <= halfPods; o4++) { // for second dimension (left-right)
   				power += coreSwitches.get(o3-1,o4-1).powerConsumption();
   			}
   		}
   
   		//---- Check Pod Switches ----//
   		for (int pod = 0; pod < numPods; pod++)	{    // for each pod			
   			for (int sw = 0; sw < numPods; sw++) {   // for each switch
   				power += podSwitches.get(pod,sw).powerConsumption();
   			}
   		}   		
   		//---- Check Servers ----//		
   		for (int pod = 0; pod < numPods; pod++) {            // for each pod		
   			for (int sw = 0; sw < halfPods; sw++) {          // for each edge-switch				
   				for (int sv = 2; sv <= halfPods + 1; sv++) { // for each server
   					power += servers.get(pod,sw,sv-2).powerConsumption();
   				}
   			}
   		}		
		return power;
	}
	
	@Override
	public void print(final PrintStream out) {
		out.println("Number of pods = " + numPods);
		
		//---- Print Core Switches ----//
		out.println("-----------------------------------------------------------------------------------------------");
		out.println("Core Layer:");		
		for (int i = 0; i < halfPods; i++){		// for first dimension (top-down)			
			for (int j = 0; j < halfPods; j++){	// for second dimension (left-right)
				out.print(coreSwitches.get(i,j).toString(printFull));
			}
			out.println();
		}
		
		//---- Print Aggregation Switches ----//
		out.println();
		out.println("-----------------------------------------------------------------------------------------------");
		out.println("Aggregation Layer:");
		for (int pod = 0; pod < numPods; pod++){			// for each pod
			out.print("Pod " + pod + " -> ");
			for (int agg = halfPods; agg < numPods; agg++){	// for each aggregate-switch
				out.print(podSwitches.get(pod,agg).toString(printFull));
			}
			out.println();
		}
		
		//---- Print Edge Switches ----//
		out.println();
		out.println("-----------------------------------------------------------------------------------------------");
		out.println("Edge Layer:");
		for (int pod = 0; pod < numPods; pod++){	// for each pod
			out.print("Pod " + pod + " -> ");
			
			for (int edge = 0; edge < halfPods; edge++){	// for each edge-switch
				out.print(podSwitches.get(pod,edge).toString(printFull));
			}
			out.println();
		}
		
		//---- Print Servers ----//
		out.println();
		out.println("-----------------------------------------------------------------------------------------------");
		out.println("Servers Layer:");
		for (int pod = 0; pod < numPods; pod++){	// for each pod
			out.print("Pod " + pod + " -> ");
			
			for (int edge = 0; edge < halfPods; edge++){    // for each edge-switch
				for (int sv = 0; sv < halfPods; sv++){      // for each server
					out.print(servers.get(pod,edge,sv).toString(printFull));
				}
			}
			out.println();
		}
	}
	
	@Override
	public void resetNumResources() {
		this.serverParams.resetNumResources();
	}

	@Override
	public void resetState(boolean state) {
		
		//---- Reset Core Switches ----//
   		for (int o3 = 1; o3 <= halfPods; o3++) {          // for first dimension (top-down)			
   			for (int o4 = 1; o4 <= halfPods; o4++) {      // for second dimension (left-right)
   				coreSwitches.get(o3-1,o4-1).resetState(state);
   			}
   		}   
   		//---- Reset Pod Switches ----//
   		for (int pod = 0; pod < numPods; pod++)	{         // for each pod			
   			for (int sw = 0; sw < numPods; sw++) {        // for each switch
   				podSwitches.get(pod,sw).resetState(state);
   			}
   		}   		
   		//---- Reset Servers ----//
   		for (int pod = 0; pod < numPods; pod++)	{          // for each pod   				
   			for (int edge = 0; edge < halfPods; edge++) {  // for each edge-switch
   				for (int sv = 0; sv < halfPods; sv++) {    // for each server
   					servers.get(pod,edge,sv).resetState(state);
   				}
   			}
   		}   		
	}
	
	@Override
	public boolean setNumResources(int numResources){
		return this.serverParams.setNumResources(numResources);
	}
}
