package network;

import java.util.Iterator;
import java.util.List;

/**
 * Represents a Multi-resource flow of packets with demands for different server resources.
 * 
 * <p>It is represented by source and target addresses and an array of resource demands.</p>
 * 
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public class Flow {
	
	/**
	 * Represent the different possible routing status of a Flow
	 */
	public static enum RoutingStatus{
		PENDING, ROUTED, ABORTED
	}
	
	//******************************** ATTRIBUTES **********************************
	private static int flowsCounter = 0; // static counter of Nodes	
	private final  int flowID;           // flow ID
	private Node source;
	private Node target;
	private float[] demands;
	private int numDemands;
	private float startTime;
	private float duration;
	private List<Node> route = null;
	private RoutingStatus status = RoutingStatus.PENDING;
	private boolean savedEndsResources = false; // if true, we have saved resources in the source and target Nodes
	
	//****************************** CONSTRUCTOR ***********************************
	/**
	 * Construct a new Flow with given demands
	 * @param source Address of the source Node of the flow
	 * @param target Address of the target Node of the flow
	 * @param demands array of Server resource demands
	 * @param startTime time at which the flow starts in seconds
	 * @param duration duration of the flow, measured in seconds
	 */
	public Flow(final Node source, final Node target, float startTime, float duration, final float[] demands){
		this.flowID     = flowsCounter++;   // get ID from counter and increment it in 1
		this.source     = source;
		this.target     = target;
		this.demands    = demands;
		this.startTime  = startTime;
		this.duration   = duration;
		this.numDemands = this.demands.length;
	}
	
	//******************************** METHODS *************************************
	/** Sets the route as aborted and return saved resources in source and target nodes */
	public void abortRouting() {
		this.status = RoutingStatus.ABORTED;
		
		if (savedEndsResources){
			source.revertResources(demands);
			target.revertResources(demands);
			savedEndsResources = false;
		}		
	}
	
	/** @return 0, if source and target Nodes have enough resources to process the flow, 
	 * or if the flow has saved resources in those Nodes. 
	 * <p>1, if the source Node does not have enough resources
	 * <p>and 2, if the target Node does not have enough resources  */
	public int checkEndsNodes(){
		if (savedEndsResources)
			return 0;
		
		if (!source.checkResources(demands))
			return 1;
		
		if (!target.checkResources(demands))
			return 2;
		
		return 0;
	}
	
	/**
	 * Checks if the flow is routable through a given Node, that is, if the Node has enough resources
	 * @param node to check
	 * @return <code>true</code> if the Node has enough resources to process the flow, <code>false</code> otherwise
	 */
	public boolean checkNode(Node node){
		if ( savedEndsResources && (node.equals(source) || node.equals(target)) ) return true;
		
		return node.checkResources(demands);
	}
	
	/** Deletes the route stored for the flow */
	public void deleteRoute() {
		this.route  = null;
		this.status = RoutingStatus.PENDING;
		this.savedEndsResources = false;
	}
	
	/**
	 * Checks if this flow has the same flowID as a given flow.
	 * <p>Note that flowID is unique as it is defined by a static counter
	 * @return <code>true</code> true of the flows have the same flowID
	 */
	@Override
	public boolean equals(Object obj){
		if ( obj == null || !(obj instanceof Flow) ) 
			return false;
		
		if( obj == this )
			return true;
		
		return this.flowID == ((Flow) obj).getFlowID();
	}
	
	/**
	 * @return as hash code the flowID of this flow
	 */
	@Override
	public int hashCode(){
		return this.flowID;
	}
	
	/** @return <code>true</code> if the flow has saved resources in its source and target nodes, 
	 * <code>false</code> otherwise  */
	public boolean hasSavedEndsResources() { return savedEndsResources;}

	/**
	 * Return the value of a specified resource-demand in the flow
	 * @param index of the resource
	 * @return the value of resource-demand
	 */
	public float getDemand(int index) {
		assertDemand(index);
		return this.demands[index];
	}
	
	/**
	 * Return the array of resource-demands of the flow
	 * @return the value of resource-demand
	 */
	public float[] getDemands() {
		return this.demands;
	}
	
	/** @return the unique ID of the Flow */
	public int getFlowID() {return this.flowID;}
	
	/**
	 * @return the number of demands of this flow
	 */
	public int getNumDemands() {return numDemands;}
	
	/** @return the route of nodes followed by the flow */
	public List<Node> getRoute() {return this.route;}
	
	/** 
	 * @return the status of the flow
	 */
	public RoutingStatus getStatus() {return status;}
	
	/** @return the source Node of the flow */
	public Node getSource()   {return this.source;}
	
	/** @return the target Node of the flow */
	public Node getTarget()   {return this.target;}
	
	/** Resets the number of demands to its original size, according to the size of the demands array */
	public void resetNumDemands() {this.numDemands = demands.length;}
	
	/**
	 * Saves resources for source and target Nodes.
	 * <p>If any of them does not have enough resource for any demand then the resources are not saved
	 * @return 0 if the method was successful, 1 if the source node does not have enough resources, 
	 * 2 if the target node does not have enough resources 
	 */
	public int saveResourcesOnEnds(){
		if (source.useResources(demands) == false) // use resources in source node
			return 1;
		
		if (target.useResources(demands) == false) // use resources in target node
			return 2;
		
		savedEndsResources = true;
		return 0;
	}
	
	/**
	 * Sets the number of demands to consider in Flows
	 * @param numDemands new number of demands to consider
	 * @return the number of demands
	 */
	public boolean setNumDemands(int numDemands) {
		if (this.numDemands < numDemands || numDemands < 0)	return false;
		
		this.numDemands = numDemands;
		return true;
	}
	
	/**
	 * Routes the flow through a sequence of Nodes from flow's source to its target
	 * <p>If we had saved resources in source and target we use them now
	 * (we are not using the same amount of resources twice)
	 * @param route sorted list of Nodes
	 * @return <code>true</code> if the route starts at source and ends at target Nodes 
	 * and there was enough resources in each node in the route,
	 * otherwise returns <code>false</code>
	 */
	public boolean setRoute(List<Node> route){
		boolean ret = false;
		
		// if the route starts at source and finishes at target:
		if (route.get(0).equals(source) && route.get(route.size()-1).equals(target)) {
			this.route  = route;                   // assign the route
			this.status = RoutingStatus.ROUTED;    // set status as routed
			ret = updateNodeResources();           // use node resources (to process flow)
			savedEndsResources = false;            // we have used the resources so now they are not saved...
		}
		return ret;
	}
	
	@Override
	public String toString(){
		String ret = toStringHead() + ", demands = [";
		
		for (int i = 0; true; i++){
			ret += demands[i];
			if (i < numDemands - 1)
				ret += " , ";
			else
				break;
		}
		ret += "]";	
		return ret;
	}
	
	/** 
	 * Returns a String representation of the head of the flow (without demands) 
	 * @return String representation of the flow
	 * */
	public String toStringHead(){
		return "(" + startTime + "-" + (startTime + duration) + "s) <"+ source.toString() 
			+ " -> " + target.toString() + ">";
	}
	
	/**
	 * Returns a String representation of the route
	 * @param includeHead boolean parameter, if <code>true</code>, 
	 * the string returned will include the route assigned to the Flow
	 * @return String representation of the flow
	 */
	public String toStringRoute(boolean includeHead){
		String ret = "";
		if (includeHead) ret += toStringHead() + ", route = ";
		
		ret += "<";

		for(Iterator<Node> iterator = route.iterator(); iterator.hasNext(); ){
			ret += iterator.next().toString();
			if (iterator.hasNext())
				ret += " , ";
			else
				break;
		}			
		ret += ">";	
		return ret;
	}
	
	//************************** PRIVATE AUXILIARY METHODS *************************
	/** Checks if a given demand index is in the valid range of demands.
	 * @param  index, index of demand to check
	 * @throws IndexOutOfBoundsException if index is not in the valid range
	 * */
	private void assertDemand(int index){
		if (index >= numDemands)
			throw new IndexOutOfBoundsException("Demand index is out of bounds, "+ index + " >= " + numDemands);
	}
	
	/** 
	 * Updates the resource values in the Nodes of the route. 
	 * @return <code>false</code> if one of the nodes does not have enough resources, otherwise return <code>true</code>
	 * */
	private boolean updateNodeResources(){
		
		for(Node node : route) {
			
			if (savedEndsResources && (node.equals(source) || node.equals(target)) )
					continue;
			
			if (node.useResources(demands) == false)
				return false;
		}
		return true;
	}
	
	
}
