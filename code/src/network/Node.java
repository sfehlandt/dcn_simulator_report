package network;


/**
 * Basic component in a network, it can represent either a Server or a Switch
 *
 * <p>{@link Server} and {@link Switch} classes are extended from Node.</p>
 *
 * @author Sebastian Fehlandt
 * @since Jul 2016
 */
public abstract class Node {
	
	//******************************** ATTRIBUTES **********************************
	/** {@link Node#CONNECTION_OK} means the port was empty and the connection was successful */
	public static final int CONNECTION_OK = 0;
	
	/** {@link Node#CONNECTION_PORT_OUT_OF_RANGE} means the port was out of range and the connection aborted */
	public static final int CONNECTION_PORT_OUT_OF_RANGE  = 1;
	
	/** {@link Node#CONNECTION_PORT_RECONNECTED} means the port was used by replaced by the new given node */
	public static final int CONNECTION_PORT_RECONNECTED = 2;
	
	private final  int    nodeID;        // node ID
	private final  int  numPorts;        // number of ports
	private int            level = 0;    // level of node
	private Address      address = null; // network address
	private boolean      onState = true; // node-device state, ON (true) or OFF (false)
	private Node[]      adjNodes = null; // set of adjacent nodes or connections (1 for each port)
	private int[]       adjPorts = null; // set of adjacent nodes or connections (1 for each port)
	private boolean[] portStates = null; // state ON/OFF of each port (adjNode). OFF (false), means no communication 
	private int       numOnPorts = 0;    // number of ports turned ON, i.e. whose portStates == true
	protected double  nodeWeight = 0;

	//****************************** CONSTRUCTOR ***********************************
	/**
     * Constructs a new Node, intended to be either a Server or a Switch
     * @param nodeID   (int) ID or index of the Node in its network.
     * @param numPorts (int) number of communication ports.
     * @param level    (int) level of the node in the network.
     * @param address  ({@link Address}) network address of the node.
     */
	public Node(int nodeID, int numPorts, int level, final Address address) {
		this.nodeID     = nodeID;
		this.numPorts   = numPorts;
		this.address    = address;
		this.adjNodes   = new    Node[this.numPorts];
		this.adjPorts   = new     int[this.numPorts];
		this.portStates = new boolean[this.numPorts];
	}
	
	//******************************** METHODS *************************************
	/**
	 * Calculates the node weight for the routing algorithm
	 * @param demands array of resource demands to be routed through the network
	 * @return the node weight calculated
	 */
	public double calculateNodeWeight(final float[] demands){return 1;}
	
	/** Checks resource availability for a complete array of demands
	 * This methods is used by Server, it is defined by Node in order to avoid subclass checking
	 * @param  demands array of resource demands to check
	 * @return <code>true</code> if each demand is lower or equal to the available resource, 
	 * otherwise returns <code>false</code>
	 * */
	public boolean checkResources(final float[] demands) {return true;}
	
	/** 
	 * Connects a given neighbour node to the given port
	 * @param  port (int) number of port or connection
	 * @param  adjPort (int) port number of the adjNode where this node is connected 
	 * @param  adjNode ({@link Node}) to connect to  
	 * @return (int) Flag indicating is the result after the operation:
	 * @see Node#CONNECTION_OK
	 * @see Node#CONNECTION_PORT_OUT_OF_RANGE
	 * @see Node#CONNECTION_PORT_RECONNECTED
	 * */
	public int connect(int port, int adjPort, final Node adjNode){
		if (port >= numPorts) // Check port within range
			return CONNECTION_PORT_OUT_OF_RANGE;
		
		int retValue;
		
		if (adjNodes[port] == null)
			retValue = CONNECTION_OK;
		else
			retValue = CONNECTION_PORT_RECONNECTED;
		
		adjNodes[port] = adjNode;
		adjPorts[port] = adjPort;
		return retValue;
	}
	
	/** @return the address of the Node in the network */
	public Address getAddress() {return address;}
	
	/** 
	 * Returns the adjacent Node connected in port i
	 * @param i number of port or connection 
	 * @return the Node connected to port i 
	 * */
	public Node getAdjacentNode(int i) {return adjNodes[i];}
	
	/** 
	 * Returns the port from the adjacent Node in which this Node is connected
	 * @param i number of port or connection 
	 * @return the port of the adjacent Node where this Node is connected 
	 * */
	public int getAdjacentPort(int i) {return adjPorts[i];}
	
	/** @return the unique ID of the Node within its own category (e.g. Servers or Switches) */
	public abstract int getDeviceID();
	
	/** @return a String indicating which type of Node is this, e.g. Server, Switch, etc */
	public String getLabel()     {return "Node";}
	
	/** @return the level of the Node in the network */
	public int getLevel()        {return this.level;}
	
	/** @return the unique ID of the Node */
	public int getNodeID()       {return this.nodeID;}
	
	/** @return the node weight for the routing algorithm */
	public double getNodeWeight() {return this.nodeWeight;}
	
	/** @return the number of ports of the Node that are currently in ON state */
	public int getNumOnPorts()   {return this.numOnPorts;}
	
	/** @return the number of ports of the Node */
	public int getNumPorts()     {return this.numPorts;}
	
	/** @return the number of Server resources, or 0 if it is a switch */
	public int getNumResources() {return 0;}
	
	/** @return <code>true</code> if the Node is in ON state, otherwise false */
	public boolean isOn()        {return this.onState;}
	
	/** 
	 * Returns true if the port i is in ON state, otherwise returns false
	 * @param  port (int) number of port or connection 
	 * @return <code>true</code> if the port is in ON state, otherwise false
	 * */
	public boolean isPortOn(int port) {return this.portStates[port];}
	
	/** @return a String representation of the list of parameters of the Server */
	public abstract String parametersToString();
	
	/** @return the current power consumption of the Server, based on its ports ON/OFF states */
	public abstract double powerConsumption();	
	
	/**
     * Resets the state of the Node
     * @param state boolean indicating if the Nodes must be reset to ON (<code>true</code>) or OFF (<code>false</code>)
     */
	public void resetState(boolean state) {
		if (state)
			turnOnFully();
		else
			turnOff();
		
		nodeWeight = 0;
	}
	
	/** Reverts a complete demand array increasing all the resources availability according to each coordinate value.
	 * @param  usages array of resource demands to allocate
	 * @return <code>false</code> if for any demand the new resource value exceeds 1 (the value will be fixed to 1)
	 * <code>true</code> otherwise.
	 */
	public boolean revertResources(final float[] usages) {return true;}
	
	@Override
	public String toString(){ return this.toString(false); }
	
	/** 
	 * Returns a String representation
	 * @param  full (boolean) number of port or connection
	 * @return full String representation of the Node if full=true, 
	 *         otherwise just the type and address 
	 * */
	public String toString(boolean full){
		
		if (!full) return this.getLabel() + " "+address.toString();
		
		String ret = "| " + this.getLabel() + " " + address.toString();
		
		for (int i = 0; i < numPorts; i++){
			String adjNodeAddress;
			
			if (adjNodes[i] == null)
				adjNodeAddress = "--------";
			else
				adjNodeAddress = adjNodes[i].getAddress().toString();
		
			ret += " | port " + i + " = " + adjNodeAddress;
		}
		return ret + " |";
	}
	
	/** Turns the Node to OFF state and all its nodes */
	public void turnOff() {
		if (!onState) return;
		
		onState = false;
		
		for (int i = 0; i < numPorts; i++)     // for each port:
			this.turnPortOff(i);               // turn OFF port on this Node
	}
	
	/** 
	 * Turns the Node to OFF state.
	 * The method checks if the adjacent Nodes are ON, for each one that is ON, 
	 * it also turns OFF the ports communicating with it
	 * (both the port of this Node and the port of the adjacent Node)*/
	public void turnOffSmart() {
		if (!onState) return;
		
		onState = false;
		
		for (int i = 0; i < numPorts; i++){     // for each port:
			this.turnPortOff(i);                    // turn OFF port on this Node
			adjNodes[i].turnPortOff(adjPorts[i]);   // turn OFF port on the adjacent Node
		}
	}
	
	/** Turns the Node to ON state. Does not turn ON any port */
	public void turnOn()  {onState = true;}
	
	/** 
	 * Turns the Node to ON state and also turns ON all the ports
	 */
	public void turnOnFully()  {
		if (onState) return;
		
		onState = true;
		
		for (int i = 0; i < numPorts; i++)      // for each port:
			this.turnPortOn(i);                 // turn ON port on this Node
	}
	
	/** 
	 * Turns the Node to ON state.
	 * The method checks if the adjacent Nodes are ON, for each one that is ON, 
	 * it also turns ON the ports communicating with it
	 * (both the port of this Node and the port of the adjacent Node)*/
	public void turnOnSmart()  {
		if (onState) return;
		
		onState = true;

		for (int i = 0; i < numPorts; i++) {     // for each port:
			if (adjNodes[i].isOn()){             // if the adjacent Node is ON:
				this.turnPortOn(i);                  // turn ON port on this Node
				adjNodes[i].turnPortOn(adjPorts[i]); // turn ON port on the adjacent Node
			}
		}
	}
	
	/** 
	 * Turns the port to OFF state  
	 * @param port (int) port number
	 * */
	public void turnPortOff(int port){
		// check if already OFF before decreasing the counter
		if (portStates[port] == true) {
			portStates[port] = false;
			numOnPorts--;
			
			if(numOnPorts == 0) onState = false; // if all ports are OFF, then turn OFF the switch
		}		
	}
	
	/** 
	 * Turns the port to ON state 
	 * @param port (int) port number
	 * */
	public void turnPortOn(int port){
		// check if already ON before increasing the counter
		if (portStates[port] == false) {
			portStates[port] = true;
			numOnPorts++;
			onState = true;
		}		
	}
	
	/** Allocates a complete demand array decreasing all the resources availability according to each coordinate value.
	 * 
	 * <p>If for any demand there is no enough resource available then the operation is cancelled, 
	 * the method returns <code>false</code>, and the resource vector is not changed (for any demand).
	 * 
	 * <p>This methods is used by Server, it is defined by Node in order to avoid subclass checking
	 * 
	 * @param  usages array of resource demands to allocate
	 * @return <code>true</code> if there is enough availability of resource, and thus, the operation was successful,
	 * <code>false</code> otherwise.
	 */
	public boolean useResources(final float[] usages) {return true;}

	
}
