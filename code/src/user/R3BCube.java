package user;

import java.io.IOException;

import sim.MRGRouting;

/**
 * Comparison 3 FatTree:
 * FatTree, 8 pods, 3 resources
 * Multi-resource algorithms, Normal vs Save Resources
 * @author sfehland
 *
 */
public class R3BCube {
	public static void main (String[] args) throws IOException {
		
		int[] numResList   = {1,2,3,4,5,6,7,8,9,10};
		int numSamples     = 20;              // number of samples
		int numFlowsSim2   = 200;             // number of flows to use in simulation
		String networkFileName = "bcubeServers_results";
		
		// List of algorithms to use:
		int[] algsList     = {
				MRGRouting.SR_SPATH,
				MRGRouting.SR_GREEN,
				MRGRouting.MR_SPATH,
				MRGRouting.MR_GREEN,
				//MRGRouting.SR_SPATH_SAVE_RESOURCES,
				//MRGRouting.SR_GREEN_SAVE_RESOURCES,
				//MRGRouting.MR_SPATH_SAVE_RESOURCES,
				//MRGRouting.MR_GREEN_SAVE_RESOURCES
				};
		
		CompareSim csim = new CompareSim();
		csim.runSimulation2(numResList, algsList, numSamples, networkFileName, numFlowsSim2);
	}
}
