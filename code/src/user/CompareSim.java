package user;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StatisticalBarRenderer;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;

import network.Session;
import sim.GUIMessenger;
import sim.MRGRouting;
import sim.Messenger;
import sim.PrintStreamMessenger;
import sim.RoutingAlgorithm;
import sim.Simulator;
import util.StaticTools;

/**
 * Object used to execute simulations sequentially and create charts with results
 * <p>Uses {@link ChartsDisplayer} to display (and perhaps save) the charts 
 * @author Sebastian Fehlandt
 * @since Aug2016
 * @see ChartsDisplayer
 */
public class CompareSim {
	
	// Variable parameters to control simulation:
	private int[] numFlowsList = {20, 40, 60, 80, 100};
	private int[] numResList   = {1,2,3,4,5,6,7,8,9,10};
	private int numSamples     = 20;              // number of samples
	private int numResSim1     = 3;               // number of resources to limit simulation 1
	private int numFlowsSim2   = 300;             // number of flows to use in simulation 2
	
	// List of algorithms to use:
	private int[] algsList     = {
			MRGRouting.SR_SPATH,
			MRGRouting.SR_GREEN,
			MRGRouting.MR_SPATH,
			MRGRouting.MR_GREEN,
			/*
			MRGRouting.SR_SPATH_SAVE_RESOURCES,
			MRGRouting.SR_GREEN_SAVE_RESOURCES,
			MRGRouting.MR_SPATH_SAVE_RESOURCES,
			MRGRouting.MR_GREEN_SAVE_RESOURCES
			*/
			};
	
	// Fixed parameters to control simulation:
	private int verbosity      = RoutingAlgorithm.VERBOSITY_ONE_LINE;
	private float mean         = (float) 0.02;    // gauss mean for flow demand generation
	private float stdDev       = (float) 0.02;    // gauu standard deviation for flow demand generation
	private String networkFileName = "fattreeServers_results";
	
	// Names of the algorithms, how they will be displayed:
	private String[] algsNames = { "MRG", "MRG(res)", "MRSP", "MRSP(res)",  "SRG", "SRG(res)", "SRSP", "SRSP(res)"};
	
	private int numCases;                     // number of cases
	private int numAlgs;                      // number of algorithms
	private int[][][] nonroutedFlowsList; // list of number of non-routed flows
	private double[][][] inactiveNodesList;  // list of number of inactive nodes
	private int[][][] congestedNodesList; // list of number of congested nodes
	private double[][][] powerList;          // list of network power consumption
	private long[][][] exectimeList;       // list of execution times
	
	private double[][] nonroutedFlowsMean; // means of non-routed flows
	private double[][] inactiveNodesMean;  // means of number of inactive nodes
	private double[][] congestedNodesMean; // means of number of congested nodes
	private double[][] powerMean;          // means of network power consumption
	private double[][] exectimeMean;       // means of execution times
	
	private double[][] nonroutedFlowsStdev; // standard deviation of non-routed flows
	private double[][] inactiveNodesStdev;  // standard deviation of number of inactive nodes
	private double[][] congestedNodesStdev; // standard deviation of number of congested nodes
	private double[][] powerStdev;          // standard deviation of network power consumption
	private double[][] exectimeStdev;       // standard deviation of execution times
	
	private JFreeChart nonroutedFlowsChart; // chart Nr of non-routed flows  VS Nr of flows
	private JFreeChart inactiveNodesChart;  // chart Nr of active nodes      VS Nr of flows
	private JFreeChart congestedNodesChart; // chart Nr of congested nodes   VS Nr of flows
	private JFreeChart powerChart;          // chart Total power consumption VS Nr of flows
	private JFreeChart exectimeChart;       // chart Execution time in [ms]  VS Nr of flows
	
	private Simulator simulator;                    // Simulator object
	private ChartsDisplayer chartsDisplayer = null; // object to display result charts
	
	//********************** CONSTRUCTORS AND RELATED METHODS **********************
	/**
	 * Creates a new CompareSim object with default parameters
	 * @throws IOException
	 */
	public CompareSim() throws IOException {
		build();
	}
	
	/**
	 * Builds the ChartsDisplayer object and executes the simulation
	 * @throws IOException
	 */
	private void build() throws IOException{
		chartsDisplayer = new ChartsDisplayer("DCN Simulation Results", Simulator.OUT_DIR);
	}

	//***************************** COMMON METHODS ******************************	
	private JFreeChart createChart(DefaultStatisticalCategoryDataset dataset, String xLabel, String yLabel, 
			double lower, double upper) {
		
		Color background = Color.white;
		
		CategoryAxis xAxis = new CategoryAxis(xLabel);		
        xAxis.setLowerMargin(0.01d);    // percentage of space before first bar
        xAxis.setUpperMargin(0.01d);    // percentage of space after last bar
        xAxis.setCategoryMargin(0.1d);  // percentage of space between categories
        ValueAxis yAxis = new NumberAxis(yLabel);

        // define the plot
        CategoryItemRenderer renderer = new StatisticalBarRenderer();
        CategoryPlot plot = new CategoryPlot(dataset, xAxis, yAxis, renderer);
        plot.setBackgroundPaint(background);
		plot.setRangeGridlinePaint(Color.gray);
		
        BarRenderer plotRenderer = ((BarRenderer) (plot.getRenderer()));
        plotRenderer.setItemMargin(0.0); 
		
		if (lower < upper) {
			plot.getRangeAxis().setRange(lower, upper);
		}

		String title = yLabel + " VS " + xLabel;
		
        JFreeChart chart = new JFreeChart(title,
                                          new Font("Helvetica", Font.BOLD, 12),
                                          plot,
                                          true);
        chart.setBackgroundPaint(background);
        chart.setBorderVisible(true);
        return chart;
	}
	
	//*********************** METHODS RELATED TO SIMULATION *************************
	/**
	 * Runs a simulation of type 1
	 * @param numFlowsList array of int with the number of flows to generate. 
	 * One simulation per value in the array
	 * @param algsList list of algorithms to use, given in int format
	 * @param numSamples number of samples to use
	 * @param networkFile name of the network file to use
	 * @param numResources number of resources to consider
	 * @throws IOException 
	 */
	public void runSimulation1(int[] numFlowsList, int[] algsList, int numSamples, String networkFile, 
			int numResources) throws IOException{
		this.numFlowsList    = numFlowsList;
		this.algsList        =  algsList;
		this.numSamples      = numSamples;
		this.networkFileName = networkFile;
		this.numResSim1      = numResources;
		runSimulation(true);
	}
	
	public void runSimulation2(int[] numResList, int[] algsList, int numSamples, String networkFile, 
			int numFlows) throws IOException{
		this.numResList    = numResList;
		this.algsList        =  algsList;
		this.numSamples      = numSamples;
		this.networkFileName = networkFile;
		this.numFlowsSim2    = numFlows;
		runSimulation(false);
	}
	
	/**
	 * Runs the simulations
	 * @param sim1 boolean indicating if the method should execute the 
	 * simulation 1 (<code>true</code>) or 2 (<code>false</code>)
	 * @throws IOException
	 */
	public void runSimulation(boolean sim1) throws IOException {		
		
		//--------- Set Simulation --------//
		numAlgs = algsList.length;    // number of algorithms
		
		if (sim1)
			numCases = numFlowsList.length; // number of flows
		else
			numCases = numResList.length;   // number of resources
		
		nonroutedFlowsList   = new    int[numCases][numAlgs][numSamples]; // list of number of non-routed flows
		inactiveNodesList    = new double[numCases][numAlgs][numSamples]; // list of number of inactive nodes
		congestedNodesList   = new    int[numCases][numAlgs][numSamples]; // list of number of congested nodes
		powerList            = new double[numCases][numAlgs][numSamples]; // list of network power consumption
		exectimeList         = new   long[numCases][numAlgs][numSamples]; // list of execution times
		
		nonroutedFlowsMean  = new double[numCases][numAlgs]; // means of non-routed flows
		inactiveNodesMean   = new double[numCases][numAlgs]; // means of number of inactive nodes
		congestedNodesMean  = new double[numCases][numAlgs]; // means of number of congested nodes
		powerMean           = new double[numCases][numAlgs]; // means of network power consumption
		exectimeMean        = new double[numCases][numAlgs]; // means of execution times
		
		
		nonroutedFlowsStdev = new double[numCases][numAlgs]; // std deviation of non-routed flows
		inactiveNodesStdev  = new double[numCases][numAlgs]; // std deviation of number of inactive nodes
		congestedNodesStdev = new double[numCases][numAlgs]; // std deviation of number of congested nodes
		powerStdev          = new double[numCases][numAlgs]; // std deviation of network power consumption
		exectimeStdev       = new double[numCases][numAlgs]; // std deviation of execution times
		

		Messenger msn       = new PrintStreamMessenger(); // create a messenger to display messages
		simulator           = new Simulator(msn);         // create a simulator
		simulator.setRoutingAlgorithm(new MRGRouting());  // Create a routing algorithm for the simulator
		simulator.buildNetwork(networkFileName);          // Create network for the simulator

		//--------- Execute Simulation --------//
		int numFlows = numFlowsSim2;
		for (int i = 0; i < numCases; i++) {
			//msn.sendMessage("Starting simulations with number of flows = " + numFlowsList[i]);
			
			for (int j = 0; j < numSamples; j++) {
				
				if (sim1) numFlows = numFlowsList[i];
				
				String msg = "";
				if (sim1)
					msg = "Nr of Flows = " + numFlows;
				else
					msg = "Nr of Resources = " + numResList[i];
				
				msn.sendMessage(msg + ", Sample " + (j+1) + " of " + numSamples);
				
				if (sim1) {
					if (numResSim1 > 0)
						simulator.getSession().setNumResources(numResSim1); // set number of resources
				} else {
					simulator.getSession().setNumResources(numResList[i]); // set number of resources
				}
				
				simulator.generateStaticTraffic(numFlows, mean, stdDev);          // generate (reset) traffic

				for (int k = 0; k < numAlgs; k++) {
					
					// Run Simulation:
					Instant start = Instant.now();                                   // measure time before
					simulator.runSimulation(algsList[k], verbosity);                 // run simulation
					Instant end   = Instant.now();                                   // measure time after
					exectimeList[i][k][j] = Duration.between(start, end).toMillis(); // calculate duration in [ms]
					
					Session ses                 = simulator.getSession();    // get Session object with results				
					nonroutedFlowsList[i][k][j] = numFlows - ses.getNumRoutedFlows();// get Nr non-routed flows				
					congestedNodesList[i][k][j] = ses.getNumNodesCongested();        // get Nr congested nodes
					powerList         [i][k][j] = ses.getPowerConsumption();         // get power of the network
					
					// Get Nr of inactive nodes [%]:
					double aux = ((double) ses.getNumNodesON()) / ((double) ses.getNumNodes());				
					inactiveNodesList [i][k][j] = (1 - aux)*100; 
				}
			}
		}
		
		//--------- Calculate Statistics --------//

		for (int i = 0; i < numCases; i++) {

			for (int k = 0; k < numAlgs; k++) {
				nonroutedFlowsMean  [i][k] = StaticTools.mean(nonroutedFlowsList[i][k]);
				inactiveNodesMean   [i][k] = StaticTools.mean(inactiveNodesList [i][k]);
				congestedNodesMean  [i][k] = StaticTools.mean(congestedNodesList[i][k]);
				powerMean           [i][k] = StaticTools.mean(powerList         [i][k]);
				exectimeMean        [i][k] = StaticTools.mean(exectimeList      [i][k]);
				
				nonroutedFlowsStdev [i][k] = StaticTools.stdDev(nonroutedFlowsList[i][k]);
				inactiveNodesStdev  [i][k] = StaticTools.stdDev(inactiveNodesList [i][k]);
				congestedNodesStdev [i][k] = StaticTools.stdDev(congestedNodesList[i][k]);
				powerStdev          [i][k] = StaticTools.stdDev(powerList         [i][k]);
				exectimeStdev       [i][k] = StaticTools.stdDev(exectimeList      [i][k]);
				
			}
		}
		
		//--------- Create Charts --------//
		createCharts(sim1);
		simulator.saveSession("CompareSimSession");
	}
	
	/**
	 * Creates the charts with results of the simulations
	 */
	private void createCharts(boolean sim1){
       
		DefaultStatisticalCategoryDataset nonroutedFlowsDataset = new DefaultStatisticalCategoryDataset();
		DefaultStatisticalCategoryDataset inactiveNodesDataset  = new DefaultStatisticalCategoryDataset();
		DefaultStatisticalCategoryDataset congestedNodesDataset = new DefaultStatisticalCategoryDataset();
		DefaultStatisticalCategoryDataset powerDataset          = new DefaultStatisticalCategoryDataset();
		DefaultStatisticalCategoryDataset exectimeDataset       = new DefaultStatisticalCategoryDataset();
		
		String xLabel = "";
		if (sim1)
			xLabel     = "Number of Flows";
		else
			xLabel     = "Number of Resources";
		
		String unroutedLabel  = "Number of Non-routed flows";
		String inactiveLabel  = "Number of Inactive nodes [%]";
		String congestedLabel = "Number of Congested nodes";
		String powerLabel     = "Power consumption [W]";
		String exectimeLabel  = "Execution Time [ms]";
		
		for (int i = 0; i < numCases; i++) {		
			for (int k = 0; k < numAlgs; k++) {
				int xValue = -1;
				
				if (sim1)
					xValue = numFlowsList[i];
				else
					xValue = numResList[i];
					
				nonroutedFlowsDataset.add( nonroutedFlowsMean[i][k], nonroutedFlowsStdev[i][k], 
						                   algsNames[algsList[k]]  , xValue+"");
				
				inactiveNodesDataset .add( inactiveNodesMean [i][k], inactiveNodesStdev [i][k], 
						                   algsNames[algsList[k]]  , xValue+"");
				
				congestedNodesDataset.add( congestedNodesMean[i][k], congestedNodesStdev[i][k], 
						                   algsNames[algsList[k]]  , xValue+"");
				
				powerDataset         .add(         powerMean [i][k], powerStdev         [i][k], 
						                   algsNames[algsList[k]]  , xValue+"");
				
				exectimeDataset      .add(      exectimeMean [i][k], exectimeStdev      [i][k], 
		                                   algsNames[algsList[k]]  , xValue+"");
			}
		}
		
		//---- Create charts ----//		
		nonroutedFlowsChart = createChart(nonroutedFlowsDataset, xLabel,  unroutedLabel, 0, 0);
		inactiveNodesChart  = createChart(inactiveNodesDataset , xLabel,  inactiveLabel, 0, 100);
		congestedNodesChart = createChart(congestedNodesDataset, xLabel, congestedLabel, 0, 0);
		powerChart          = createChart(powerDataset         , xLabel,     powerLabel, 0, 0);
		exectimeChart       = createChart(exectimeDataset      , xLabel,  exectimeLabel, 0, 0);
		
		chartsDisplayer.setCharts(nonroutedFlowsChart, inactiveNodesChart, congestedNodesChart, powerChart);
	}
	

	//******************************** MAIN METHOD *********************************
	/**
	 * Main method, creates a menu and executes the simulations
	 * @param args arguments for the main method
	 * @throws IOException
	 */
	public static void main (String[] args) throws IOException{
		CompareSim csim = new CompareSim();
		csim.runSimulation(true);
	}
}
