\chapter{Evaluation} \label{res}
%\textcolor{red}{
%Results, Analysis and Evaluation: \\
%- Results have been analysed and discussed \\
%- Analysis and evaluation procedures have been explained \\
%- Material has been compared and contrasted \\
%- Comparison of achievements with requirements \\
%- Awareness of potential extensions \\
%- Reflective and justified conclusions have been drawn \\
%}

\section{Methodology}\label{res:met}
This section provides the methodology of how the software is evaluated. Since the project implements one of the algorithms proposed in \cite{wang_multi-resource_2015}, the evaluation is done under similar conditions.

\subsection{Random resource demands}\label{res:met:norm}
The authors of \cite{wang_multi-resource_2015} evaluate the algorithms using random flow demand values that follow a normal distribution with mean and standard deviation values of 0.02 \cite{wang_multi-resource_2015}. Nevertheless, such probability distribution might lead to negative values, or even values greater than 1. In order to cope with this, the \code{genRandomTraffic()} method of the \code{Session} class, uses a pseudo-random number generator and checks if the value of the generated number is in the range $[0,1]$. If the number exceeds this range, a new number is generated until a number in the range $[0,1]$ is obtained.

\begin{figure}[H]
\centering
\subfigure{\includegraphics[width=0.7\textwidth]{figures/random_dist_current.png}}
\caption{Distribution of 5000 values generated to constitute flow demands}
\label{fig:res:rand}
\end{figure}

The aforementioned operation is checked using the \code{HistogramTester} class of the \code{test} package, as explained in section \ref{imp:imp:test}. This test builds a \code{Session} with a fat-tree of 8 pods and 10 resource dimensions, and generates a traffic of 500 flows. Then plots in an histogram the demand values of all these flows (5000 values in total) to show its distribution curve. This result is shown in figure \ref{fig:res:rand}.

As can be seen in figure \ref{fig:res:rand}, the random values resemble a truncated normal distribution centred in 0.02. As expected, the negative portion of the curve is truncated as demand values cannot be negative. This confirms the quality of the pseudo-random number generator.

\subsection{Simulation settings}\label{res:met:settings}
Results are obtained using two networks: a fat-tree and a BCube, both using servers as switching devices. The parameters of the networks are dimensioned in order to make them have roughly the same number of total nodes. In order to compare the results to those obtained in \cite{wang_multi-resource_2015}, the fat-tree used has 8-pods, leading to a total of 208 nodes, 80 packet-processing hosts and 128 end-hosts. In order to match these numbers, a level 2 BCube with 5 port switches is used, leading to a total of 200 nodes, 75 packet-processing hosts and 125 end-hosts. The network files used for these simulations are \code{fattreeServers\_results.csv} and \code{bcubeServers\_results.csv}, and are available in the \code{user\_files/net} directory.

For both networks, two type of simulations are executed:

\begin{enumerate}
\item Variation of the number of flows: simulations are run for different for different number of flows in order to analyse how the results change as the network gets more congestion. In these simulations the number of resource dimensions used is 3.

\item Variation of the number of resource dimension: simulations are run for different numbers of resource dimensions. In these simulations the number of flows to route is fixed on 200. In order to do this, the \code{Network} object is created with the maximum number of resources to consider, which is 10. Then, the number of resources is limited by executing the method \code{setNumResources()} of the \code{Session} class.
\end{enumerate}

All the cases are sampled 20 times, and the mean and standard deviation of the results are plotted in statistical bar charts, using the \code{CompareSim} class, as mentioned in section \ref{imp:use:ext}.

The results displayed are the same as in \cite{wang_multi-resource_2015}, in order to compare them. These include: 

\begin{itemize}
\item Number of non-routed or aborted flows: this represents a measure of effectiveness of the algorithm, as all the flows should be routed in an ideal scenario.

\item Percentage of active nodes: this represents a measure of efficiency, since the algorithms follow a greedy scheme, activating as few nodes as possible.

\item Number of congested nodes: this represents a measure of efficiency. However, the measure of congestion is not trivial in a multi-resource approach, as a node can get congested in one of its resource dimensions. Furthermore, if a node is considered congested when one of its residual resources reaches zero, then it is virtually impossible that a node gets congested in the algorithm. This is due to the nature of the algorithm. Indeed, the algorithm routes a given flow through a node only if the node has enough resources in all its dimensions to satisfy the flow demands. Hence, it is really unlikely that any resource reaches the zero value, as the sum of every demand value of all the flows assigned to the node must be exactly 1.

Therefore, in order to provide a more realistic measure of congestion, it is measured in function of the non-routed flow demands. That is, a node is congested if it is active, and none of the non-routed flows can be routed through it, regardless of the reachability of the node by the flow.

\item Power consumption: is measured in watts ([W]). The work presented in \cite{wang_multi-resource_2015} does not provide a measure of direct power consumption, so no comparison can be made. However, the results are shown here as complimentary data.
\end{itemize}

It is worth to mention that the power consumption depends heavily on the power consumption parameters used for servers and switches. These parameters have been taken from \cite{shang_network_2015}. However, they have not been checked against other information, and thus, they do not constitute definitive measures of power consumption.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                    RESULTS
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results and Analysis}\label{res:res}
This section presents and analyses the results obtained using the simulation settings described in the previous section. In order to facilitate the understanding of the charts, an explanation of the labels is shown below:

\begin{itemize}
\item MRG:  Multi-resource green algorithm
\item MRSP: Multi-resource shortest-path algorithm
\item SRG:  Single-resource green algorithm
\item SRSP: Single-resource shortest-path algorithm
\item MRG(res):  Multi-resource green algorithm with resource pre-allocation
\item MRSP(res): Multi-resource shortest-path algorithm with resource pre-allocation
\item SRG(res):  Single-resource green algorithm  with resource pre-allocation
\item SRSP(res): Single-resource shortest-path algorithm  with resource pre-allocation
\end{itemize}

\newpage
\subsection{Impact of the pre-allocation of resources}\label{res:res:save}
Firstly, a comparison between the different multi-resource algorithms is presented. This is done to explore the impact of the pre-resource allocation in flows source and target nodes, as described in section \ref{imp:alg:modes}. The results are displayed in figures \ref{fig:res1fattree} and \ref{fig:res1bcube}.

\begin{figure}[H]
\begin{center}
%
\subfigure[Number of non-routed flows]{%
    \label{fig:first}
    \includegraphics[width=0.5\textwidth]{figures/res/c1_fattree/Chart1.png}
}%
\subfigure[Percentage of inactive nodes]{%
   \label{fig:second}
   \includegraphics[width=0.5\textwidth]{figures/res/c1_fattree/Chart2.png}
}\\ %  ------- End of the first row ----------------------%
\subfigure[Number of congested nodes]{%
    \label{fig:third}
    \includegraphics[width=0.5\textwidth]{figures/res/c1_fattree/Chart3.png}
}%
\subfigure[Power consumption]{%
    \label{fig:fourth}
    \includegraphics[width=0.5\textwidth]{figures/res/c1_fattree/Chart4.png}
}%
%
\end{center}
\caption{Impact of the pre-resource allocation on a Fat-tree with 8 pods}%
\label{fig:res1fattree}
\end{figure}

\begin{figure}[H]
\begin{center}
%
\subfigure[Number of non-routed flows]{%
    \label{fig:first}
    \includegraphics[width=0.5\textwidth]{figures/res/c1_bcube/Chart1.png}
}%
\subfigure[Percentage of inactive nodes]{%
   \label{fig:second}
   \includegraphics[width=0.5\textwidth]{figures/res/c1_bcube/Chart2.png}
}\\ %  ------- End of the first row ----------------------%
\subfigure[Number of congested nodes]{%
    \label{fig:third}
    \includegraphics[width=0.5\textwidth]{figures/res/c1_bcube/Chart3.png}
}%
\subfigure[Power consumption]{%
    \label{fig:fourth}
    \includegraphics[width=0.5\textwidth]{figures/res/c1_bcube/Chart4.png}
}%
%
\end{center}
\caption{Impact of the pre-resource allocation on a level 2 BCube with 5 ports per switch}%
\label{fig:res1bcube}
\end{figure}

The results presented in figures \ref{fig:res1fattree} and \ref{fig:res1bcube} reflect that the impact of the pre-allocation of resources does not affect the overall results of the algorithm in terms of energy efficiency. Moreover, in both networks, the percentage of active nodes practically does not change when comparing either algorithm. That is, the results of the multi-resource shortest-path algorithm does not change when introducing the resource pre-allocation. The same happens when comparing the results for the multi-resource green algorithm with and without pre-allocation of resources.

In the case of the Fat-tree, the results show absolutely no difference between the regular versions of the algorithms and the variations with resource pre-allocation, as shown in figure \ref{fig:res1fattree}. This is explained by the fact that the end-hosts in a fat-tree do not contribute to data forwarding and routing, and thus, they can only be source or target nodes on a flow, otherwise they will never be part of the route.

In the case of the BCube, on the contrary, a difference can be appreciated in the number of non-routed flows and number of congested nodes, as shown in figure \ref{fig:res1bcube}. For heavy traffic loads, over 400 flows, the multi-resource green presents more non-routed flows and less congested nodes than its variation with pre-allocation of resources. This reflects that the idea behind the resource pre-allocation variation is correct. Under heavy load scenarios in a BCube, the pre-allocation of resources can effectively reduce the number of non-routed flows and does not show a negative impact on energy-efficiency.

Additionally, the results show that in both cases the green algorithms are more energy-efficient than the shortest-path algorithms as expected, as they have a higher rate of inactive nodes. The improvement of the green algorithms over the shortest-path ones increases with the number of flows. This is specially noticeable in the BCube, where even from low traffic the green algorithm is clearly more energy-efficient. The drawback of the green algorithms is the impact on non-routed flows on high traffic conditions. In both networks, the green algorithms showed more non-routed flows, specially in the BCube where the shortest-path algorithms presented almost no non-routed flows. Nonetheless, this becomes clear with extremely high traffic conditions, around 800 flows for a total of 200 nodes.

As the impact of the resource pre-allocation is negligible, it is not considered in the rest of the analysis, and those variations are left out of the charts of the following subsections.

\newpage
\subsection{Multi-Resource vs Single-Resource}\label{res:res:mr_vs_sr}
This section presents a comparison between the single-resource and multi-resource versions of both: the green and shortest-path algorithms. The results are displayed in figures \ref{fig:res2fattree} and \ref{fig:res2bcube}.

\begin{figure}[H]
\begin{center}
%
\subfigure[Number of non-routed flows]{%
    \label{fig:first}
    \includegraphics[width=0.5\textwidth]{figures/res/c2_fattree/Chart1.png}
}%
\subfigure[Percentage of inactive nodes]{%
   \label{fig:second}
   \includegraphics[width=0.5\textwidth]{figures/res/c2_fattree/Chart2.png}
}\\ %  ------- End of the first row ----------------------%
\subfigure[Number of congested nodes]{%
    \label{fig:third}
    \includegraphics[width=0.5\textwidth]{figures/res/c2_fattree/Chart3.png}
}%
\subfigure[Power consumption]{%
    \label{fig:fourth}
    \includegraphics[width=0.5\textwidth]{figures/res/c2_fattree/Chart4.png}
}%
%
\end{center}
\caption{Impact of the multi-resource approach on a Fat-tree with 8 pods}%
\label{fig:res2fattree}
\end{figure}

\begin{figure}[H]
\begin{center}
%
\subfigure[Number of non-routed flows]{%
    \label{fig:first}
    \includegraphics[width=0.5\textwidth]{figures/res/c2_bcube/Chart1.png}
}%
\subfigure[Percentage of inactive nodes]{%
   \label{fig:second}
   \includegraphics[width=0.5\textwidth]{figures/res/c2_bcube/Chart2.png}
}\\ %  ------- End of the first row ----------------------%
\subfigure[Number of congested nodes]{%
    \label{fig:third}
    \includegraphics[width=0.5\textwidth]{figures/res/c2_bcube/Chart3.png}
}%
\subfigure[Power consumption]{%
    \label{fig:fourth}
    \includegraphics[width=0.5\textwidth]{figures/res/c2_bcube/Chart4.png}
}%
%
\end{center}
\caption{Impact of the multi-resource approach on a level 2 BCube with 5 ports per switch}%
\label{fig:res2bcube}
\end{figure}

As shown in figures \ref{fig:res2fattree} and \ref{fig:res2bcube}, in both architectures the results are similar with respect to energy efficiency. In both cases the most advantageous algorithm is the multi-resource green algorithm, followed by the single-resource green algorithm. Also, the results of both shortest-path algorithms are the same. This is explained by the fact that they are essentially the same algorithm, as none of them performs the first search for a candidate flow that can be routed in the subset of active nodes, nor uses the inversion-based node weight assignment.

Likewise in the previous section, Fat-tree exhibits similar results with all the algorithms at low traffic, and as the number of flows increases, the differences become clearer. Also, the number of non-routed flows becomes evident at 800 flows, and the number of non-routed flows is slightly higher in the green algorithms. BCube, on the other hand, presents better results with the multi-resource green algorithm even at low traffic load. However, at the highest traffic evaluated, almost all the nodes are activated regardless of the algorithm used. While in the fat-tree, the multi-resource green algorithm still outperforms the others.

With respect to the non-routed flows and congested nodes, the scenario is the same as in the previous section: they occur only at extremely high traffic rates. In Bcube the shortest-path algorithms present almost no non-routed flows and congested nodes, while the green algorithms show an important number of them. Meanwhile in Fat-tree, the number of non-routed flows is consistent among the algorithms, being around 10\% higher in the green routing algorithms. Both algorithms presented almost no congested nodes. In Fat-tree there were no congested nodes, while in BCube the number is low (7 in the worst scenario), and the variance is too high to make any conclusion.

This phenomenon of more non-routed flows in the green algorithms can be explained by the fact that these algorithms restrict the search in first instance to the subset of active nodes, while the shortes-path algorithms consider all the nodes. Therefore, it is reasonably, that the green algorithms tend to congest some critical nodes when the traffic is high, as they are designed to exploit active paths rather than explore new ones.

A possible solution to this problem could be a re-routing scheme for conflictive flows. This means, identify the nodes that produce the bottleneck and re-route some of the flows routed through them.

\newpage
\subsection{Impact of the number of resource dimensions}\label{res:res:res}
This section presents an analysis of the impact of the number of resource dimensions on the results. The results are displayed in figures \ref{fig:res3fattree} and \ref{fig:res3bcube}.

\begin{figure}[H]
\begin{center}
%
\subfigure[Number of non-routed flows]{%
    \label{fig:first}
    \includegraphics[width=0.5\textwidth]{figures/res/c3_fattree/Chart1.png}
}%
\subfigure[Percentage of inactive nodes]{%
   \label{fig:second}
   \includegraphics[width=0.5\textwidth]{figures/res/c3_fattree/Chart2.png}
}\\ %  ------- End of the first row ----------------------%
\subfigure[Number of congested nodes]{%
    \label{fig:third}
    \includegraphics[width=0.5\textwidth]{figures/res/c3_fattree/Chart3.png}
}%
\subfigure[Power consumption]{%
    \label{fig:fourth}
    \includegraphics[width=0.5\textwidth]{figures/res/c3_fattree/Chart4.png}
}%
%
\end{center}
\caption{Impact of the number of resource dimensions on a Fat-tree with 8 pods}%
\label{fig:res3fattree}
\end{figure}

\begin{figure}[H]
\begin{center}
%
\subfigure[Number of non-routed flows]{%
    \label{fig:first}
    \includegraphics[width=0.5\textwidth]{figures/res/c3_bcube/Chart1.png}
}%
\subfigure[Percentage of inactive nodes]{%
   \label{fig:second}
   \includegraphics[width=0.5\textwidth]{figures/res/c3_bcube/Chart2.png}
}\\ %  ------- End of the first row ----------------------%
\subfigure[Number of congested nodes]{%
    \label{fig:third}
    \includegraphics[width=0.5\textwidth]{figures/res/c3_bcube/Chart3.png}
}%
\subfigure[Power consumption]{%
    \label{fig:fourth}
    \includegraphics[width=0.5\textwidth]{figures/res/c3_bcube/Chart4.png}
}%
%
\end{center}
\caption{Impact of the number of resource dimensions on a level 2 BCube with 5 ports per switch}%
\label{fig:res3bcube}
\end{figure}

As shown in figures \ref{fig:res3fattree} and \ref{fig:res3bcube}, the results for different numbers of resource dimensions remain constant regardless of the algorithm. The results are consistent with the previous sections (please note that the scale has been zoomed-in in the charts displayed the percentage of inactive nodes). The only singularity is a non-routed flow and congested node in the BCube with one resource dimension using the multi-resource green algorithm, which is reasonable since the inversion-based node weight assignment used by the algorithm is not designed to work with one resource dimension. Nevertheless, the variance is too high, and thus it is only an extreme case which should be ignored.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%               COMPARISON TO PAPER
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{Comparison with Wang et all (\cite{wang_multi-resource_2015})}\label{res:paper}
This section presents a comparison of the results obtained with the results presented in \cite{wang_multi-resource_2015}. It is worth to mention that the authors of that work evaluated their implementation only using Fat-trees, and hence, it is not possible to compare the results obtained for the BCube. 

The results obtained in \cite{wang_multi-resource_2015} are shown in figure \ref{fig:res:paper1}, and correspond to a Fat-Tree of 8-pods and 3 resource dimensions.

\begin{figure}[H]
\centering
\subfigure{\includegraphics[width=0.8\textwidth]{figures/paper_1.png}}
\caption{Results obtained by \cite{wang_multi-resource_2015}. Obtained from \cite{wang_multi-resource_2015}}
\label{fig:res:paper1}
\end{figure}

Comparing the results displayed in figures \ref{fig:res2fattree} and \ref{fig:res:paper1}, it is clear that there are important differences in the results. Firstly, for all the algorithms, the implementation of \cite{wang_multi-resource_2015} starts with lower values of active nodes than the implementation presented here, but it converges to steadier values faster.  Secondly, the implementation of \cite{wang_multi-resource_2015}, presents non-routed flows and congested nodes much earlier.

These differences reflect that the implementation of this project is more successful at routing flows with higher traffic loads, but is less efficient as it activates more nodes. This is a reasonable behaviour, since when facing a higher traffic load, the implementation presented here manages to find a solution but at a higher cost.

On the other hand, when comparing the results shown in figures \ref{fig:res3fattree} and \ref{fig:res:paper1}, it is observerd that this implementation does not show major differences with respect to the number of resource dimensions. This contrasts with the increasing tendency shown in \cite{wang_multi-resource_2015} for resource dimensions in the range $\{1,5\}$. In this test, 200 flows were used, while in \cite{wang_multi-resource_2015} it is not specified. However, it is likely they used a smaller value, since overall rates of inactive nodes are higher.

Unfortunately the implementation details of \cite{wang_multi-resource_2015} are not presented in the paper, and therefore, it is difficult to find the cause of these differences. However, from the point of view of a real world application, the behaviour of this implementation is preferably, as it discards less data processing requests in the network.

