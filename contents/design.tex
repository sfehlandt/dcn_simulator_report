\chapter{Objectives, Specification and Design}\label{des}

%\textcolor{red}{
%For artefact-producing project: \\
%- Requirements/objectives have been described adequately \\
%- A rationale for the design approach is given \\
%- Alternative designs have been considered \\
%

\section{Objectives and Specification} \label{des:obj}
As stated in \ref{intro:obj}, the software must be able to model different \glspl{dcn} architectures and simulate their operation under a multi-resource power-aware routing algorithm. This includes models for network components such as servers and switches, modelling of multi-resource requests to be routed on the network, and the routing algorithm to define the routes for those request across the network.

In addition, it is important that the software is extendible, in order to incorporate more routing algorithms, software components and \glspl{dcn} architectures in the future. Therefore, the software design must be clear and modular.

\subsection{Requirements} \label{des:obj:req}
The previous objectives are summarised in the following list of requirements:

\begin{itemize}
\item Model of network components, namely, servers and switches
\item Model of network, composed by servers and switches interconnected according to particular architectures
\item Model of multi-resource requests of processing in the network, according to the \gls{naas} approach.
\item Routing algorithm to route requests through the network
\item Simulator module capable of executing simulations in a network using a routing algorithm
\item Results visualisation module
\item Modularity
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%  DESIGN  %%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Design of the Software} \label{des:design}
The software is developed under the \gls{oom} approach using the Java programming Language. This decision was made because \gls{oom} allows a modular approach to model software components, where real world entities are modelled as Java objects, and new objects can be extended from existing ones through inheritance. Also, the specification of interfaces that provide particular functionalities allows a consistent modular design, where objects can be replaced by others able to provide those functionalities through the implementation of the interface.

For further explanation about the decision of using the Java programming language please see section \ref{imp:gen:ide}.

\subsection{Software Architecture} \label{des:design:arch}
Figure \ref{fig:des:arch} shows the architecture diagram, showing the conceptual model behind the software.

\begin{figure}[!ht]
\centering
\subfigure{\includegraphics[width=\textwidth]{figures/uml_arch.png}}
\caption{UML Architecture Diagram}
\label{fig:des:arch}
\end{figure}

As shown in figure \ref{fig:des:arch}, the \comp{Simulator} component requires a \comp{Network}, a \comp{RoutingAlgorithm} and a \comp{Messenger} interfaces and provides simulating functionalities through the \comp{Simulator} interface. The functionalities provided by these components are summarised below:

\begin{itemize}
\item \comp{NetworkBuilder} provides the \comp{Network} interface, i.e. provides a Network object to be used.

\item \comp{Flow} represents a multi-resource data-flow request to be routed through the \comp{Network}.

\item \comp{Session} contains the \comp{Network} and the \comp{Flow}s to be routed through it, and provides functionality to manipulate them to the \comp{Simulator}.

\item \comp{RoutingAlgorithm} provides a routing algorithm to be used by the simulator in order to route flows through the \comp{Network}.

\item \comp{Messenger} provides methods to display messages, warnings and errors to the user.

\item \comp{FileManager} provides functionality to read and write files. Here it is presented as a single component, but the implementation uses two different libraries to read/write two different types of files: \code{.csv} files with parameters to construct a \comp{Network} and/or execute a simulation, and \code{.xlm} files storing the serialisation of a complete \comp{Session} component.

\item \comp{Simulator} provides simulating functionality. It is intended to be used in two ways: as a standalone program whose input is configured by a \code{.csv} file, or as a software component to be used by other programs, allowing different users to build programs that make use of the simulator in different ways. Chapter \ref{imp} provides an example of this use. 
\end{itemize}

\subsection{Class Model} \label{des:design:class}
The previous section presented a conceptual model of the architecture of the software tool, which shows the relationship between the different components of the software. This section presents a more detailed class model of the software.

The main classes of the project can be classified in two groups: network classes, containing all the objects related to networks and their operation as can be seen in figure \ref{fig:des:class_net}; and simulator classes, containing the classes that are more related to the execution of simulations as can be seen in figure \ref{fig:des:class_sim}.

\subsubsection{Network Class Model} \label{des:design:class:net}

\begin{figure}%[!ht]
\centering
\subfigure{\includegraphics[width=1.1\textwidth]{figures/class_diag_network.png}}
\caption{Class diagram of network elements}
\label{fig:des:class_net}
\end{figure}

As shown in figure \ref{fig:des:class_net}, the main network devices \comp{Server} and \comp{Switch} are implemented as extensions of the abstract class \comp{Node}. Two main \comp{Network} classes are implemented, \code{FatTree} and \code{BCube}, corresponding to Fat-tree and BCube \gls{dcn} architectures respectively. Both classes contain sets of nodes and are built by interconnecting the nodes according to their corresponding architecture design.

Each \comp{Node} has an \comp{Address} object that represents its address in the network. Also, each \comp{Node} has an array of references to the other \comp{Node}s to which it is interconnected (\code{connections}). The index of each connection in the array corresponds to the port where the connection is made in the \comp{Node}. Connections are made by calling the method \code{connect()}. \comp{Node} also defines abstract methods that must be implemented by both \comp{Server} and \comp{Switch}.

The main difference between \comp{Server} and \comp{Switch} objects is that the former has resources to apply in-network processing. These resources are represented by an array (\code{resources}) of values between 0 and 1, according to the formulation described in \cite{wang_multi-resource_2015}. Both \comp{Server} and \comp{Switch} objects need different parameters to model certain characteristics, such as their power consumption. As stated by \cite{wang_multi-resource_2015}, it can be assumed that all the servers and switches are equal since most \glspl{dcn} use commodity servers and switches. This property is exploited in the software by the implementation of \comp{ServerParameters} and \comp{SwitchParameter} classes to store these parameters, allowing memory savings by storing one \comp{ServerParameters} and one \comp{SwitchParameter} object in the \comp{Network} object, and sharing them among all the corresponding devices. This prevents duplication of equal and invariable data in \comp{Server} and \comp{Switch} objects within the same \comp{Network}. Of course, this depends completely of the construction of network, so future network objects can be implemented with different classes of servers and/or switches, sharing or not different sets of parameters.

\comp{Flow} objects on the other hand, have an array of resource demands (\code{demands}), references to \code{target} and \code{source} \comp{Node}s, and also a set of \comp{Node}s corresponding to the route found by the algorithm (\code{route}).

Additionally, since the \gls{naas} approach considers processing along the route in the network \cite{wang_multi-resource_2015}, the software need implementations of the networks that use servers for packet forwarding and processing, instead of switches. This is provided by the subclasses \comp{FatTreeOnlyServers} and \comp{BCubeOnlyServers}, which are basically the same as their parent classes (\comp{FatTree} and \comp{BCube} respectively), but instead of using \comp{Switch} objects they use \comp{Server} objects.

The \comp{Session} class contains a reference to a \comp{Network} interface and a collection of \comp{Flow}s. As all the networks implement the \comp{Network} interface any of them can be assigned to the \comp{Session}. \comp{Session} receives the \comp{Network} object through its constructor, while the flows are generated through the \code{genNormalStaticTraffic()} and \code{genNormalTraffic()} methods.


\subsubsection{Network Class Model} \label{des:design:class:sim}

\begin{figure}%[!ht]
\centering
\subfigure{\includegraphics[width=1.05\textwidth]{figures/class_diag_sim.png}}
\caption{Class diagram of simulator elements}
\label{fig:des:class_sim}
\end{figure}

As shown in figure \ref{fig:des:class_sim}, the main class is \comp{Simulator}, which references to a \comp{Session} object, which in turn has a reference to a \comp{Network}. \comp{Simulator} also is associated to \comp{RoutingAlgorithm} and \comp{Messenger} interfaces, which define methods for executing routing algorithms and display messages, respectively. The multi-resource green routing algorithm presented in \cite{wang_multi-resource_2015} is implemented in the \comp{MRGRouting} class, which implements \comp{RoutingAlgorithm} interface. Additionally, the \comp{PrintStreamMessenger} is provided as a simple implementation of the \comp{Messenger} interface that displays the messages to a \code{PrintStream} object, such as the Java \code{System.out} stream.

In order to facilitate the incorporation of different Network topologies, the model includes a static class \comp{NetworkBuilder} that defines only one static method \code{buildNetwork}, which receives the needed parameters to construct a network as arrays of \code{Strings} and returns a \comp{Network} object. To identify which \code{Network} implementation must be returned, the method reads the content of a String parameter called \code{arch}. The value of this parameter is compared with the static fields described by the \comp{NetworkBuilder} class. Each of these fields correspond to a different network topology. With this approach, future \comp{Network} objects can be included by just modifying the \comp{NetworkBuilder} object and not the \comp{Simulator} class.