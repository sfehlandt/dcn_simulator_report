\chapter{Background} \label{bgnd}

\section{Data Centre Networks} \label{bgnd:dcn}
\Glspl{data centre} are basically facilities hosting several servers that provide different services to the users or clients of the \gls{data centre}, such as storage, CPU processing, memory, network, bandwidth, among others \cite{bilal_quantitative_2013,mustafa_performance_2015}. Servers are interconnected to form what is called a \gls{dcn}, which is designed according to a particular \gls{dcn} architecture or topology. Servers communicate with each other by sending data flows through the network according to different communication patterns depending on the amount of origins and destinations, such as one-to-one, one-to-all or all-to-all \cite{chen_survey_2011}.

This section provides a brief review of the main concepts related to \gls{dcn} architectures, the main architectures or topologies, and metrics to evaluate the performance of the network. This provides a basic background to understand the project and familiarise with the \gls{dcn} architectures used to test it.

%***********************************************************
%************************  Metrics  ************************
%***********************************************************
\subsection{Metrics} \label{bgnd:dcn:metrics}
This section provides basic definitions for the metrics that were found recurrently during this revision.

\subsubsection{Bandwidth, throughput and bisection bandwidth} \label{bgnd:dcn:bandwidth}
The \gls{bandwidth} is the maximum bit rate at which information is propagated through the network or through a link in the network. \Gls{throughput}, on the other side, is the real propagation rate at the network \cite{hennessy_computer_2002}. That means, \gls{bandwidth} is a constant maximum measure, while \gls{throughput} depends on the actual operation of the network. A bisection of a graph or network is an arbitrary imaginary subdivision in two partitions with roughly the same size, that means, both partition have almost the same number of nodes. The \gls{bisection bandwidth} is the worst-case aggregated bandwidth of all the links that connect the partitions of the bisection \cite{hennessy_computer_2002}. These three metrics are measured in bits per second.

\subsubsection{End-to-end delay} \label{bgnd:dcn:delay}
The \gls{ntn delay} is the total time that takes for a packet sent by a server to arrive to its destination. It is composed by all the delays experienced for the packets, namely: processing delay, transmission delay and queueing delay \cite{li_fcell:_2015}. Processing delay is the time required to examine a packet's head and determine where to direct it. The transmission delay is the time spent on the transmission of the packets, and queueing delay is the delay caused by congestion in switches and routers, which depends on the operation conditions of the network \cite{li_fcell:_2015}.

\subsubsection{Oversuscription} \label{bgnd:dcn:oversubs}
The \gls{oversubscription} is a measure that indicates the proportion of the total bandwidth available for any communication pattern. It is defined as the ratio of the worst-case aggregate \gls{bandwidth} among host ends to the total \gls{bisection bandwidth} of a network \cite{al-fares_scalable_2008}. For example, an \gls{oversubscription} ratio of 1:1 means that all servers are able to communicate with any other server within the network at full \gls{bandwidth}, while an \gls{oversubscription} of 5:1 means that at least 20\% of the \gls{bandwidth} is available for some communication patterns \cite{al-fares_scalable_2008}. In general, \glspl{dcn} with high oversubscription have worse worst-case communication \gls{bandwidth}, but also less links, and thus they are cheaper.

\subsubsection{Unified path length and diameter} \label{bgnd:dcn:unidiam}
In \cite{li_fcell:_2015} the authors define two interesting metrics, aimed to provide unified definitions that work for switch-centric, server-centric and hybrid \glspl{dcn}. The \gls{unified path length} $d_P$ is the length of any path $P$ in the network, which can be composed by any number of servers and switches. The \gls{network diameter} $d$ is the length of the maximum shortest-path between any pair of servers. These metrics are defined as follows:
\begin{gather}
d_P = n_{P,w}d_w + (n_{P,w} + 1)d_v \\
d = \max_{P \in \mathcal{P}} d_P
\end{gather}

Where, $\mathcal{P}$ is the set of shortest-paths between all pairs of servers in the \gls{dcn}, $n_{P,w}$ and $n_{P,v}$ are the number of switches and servers in path $P$ respectively, $d_w$ and $d_v$ are the total delay times of switches and servers.

\subsubsection{Network power efficiency} \label{bgnd:dcn:npe}
The \gls{npe} is a metric introduced in \cite{shang_network_2015}, it is defined as the total network throughput divided by the total power consumption and is measured in [bps/Watt]. The calculation of this metric depends on the power consumption model used for the \gls{dcn}. In this particular work the authors assume \gls{soi} and \gls{woa} for switch ports, that means, ports can be put to sleep when not in use and awaken when required in order to save energy. They also assume that switches can be put to sleep when all their ports are sleeping, leading to no power consumption in such cases. With this consideration they define the power consumption as:
\begin{gather}
P_{tot} = \sum_i(U_i) + \sum_j(V_j) + \sum_c(E_c*Y_c) \\
U_i =
\left\{
	\begin{array}{ll}
		0                     & \mbox{if } C_i = M_i \\
		T_i + (C_i - M_i)*Q_i & \mbox{if } C_i \ge M_i
	\end{array}
\right.
\end{gather}

Where $U_i$ and $V_j$ are the power consumption of switch $i$ and server NIC port $j$ respectively, $E_c$ is the power of server core $c$ used for network processing and forwarding and $Y_c$ is the utilization ratio of the core. $T_i$ is the base power consumption, $C_i$ and $M_i$ are the total and idle number of ports and $Q_i$ is the power consumption of one port.

%***********************************************************
%*********************  Architectures  *********************
%***********************************************************
\subsection{Architectures} \label{bgnd:dcn:arch}

%*********************      Types      *********************
\subsubsection{Types of architectures} \label{bgnd:dcn:arch:types}
\gls{dcn} architectures can be classified among three types \cite{bilal_quantitative_2013,popa_cost_2010,bilal_green_2013,shang_network_2015}:

\paragraph{Switch-centric:} \label{bgnd:dcn:arch:sw}
Switch-centric \gls{dcn} architectures rely on network switches to perform communication and routing in the network.

\paragraph{Server-centric:} \label{bgnd:dcn:arch:sv}
In server-centric \gls{dcn} architectures, the servers are not only used to perform tasks but also for routing and forwarding packages in the network. This type of architecture does not use routers of switchers.

\paragraph{Hybrid:} \label{bgnd:dcn:topo:hb}
Hybrid \gls{dcn} architectures mix both switches and routers along with servers. They may also include optical and/or wireless devices \cite{bilal_quantitative_2013}.

%*********************   Typical     ************************
\subsubsection{Typical architectures} \label{bgnd:dcn:arch:typical}
This section contains a brief review of the more important current \gls{dcn} architectures.

\paragraph{Three-tier} \label{bgnd:dcn:3t}
The three-tier architecture is a switch-centric architecture structured as tree composed by three layers of switches: access or edge layer, aggregate layer and core layer. Servers are connected to the switches in the access layer, which are connected to switches in the aggregation layer. Core layer switches are connected to all the aggregation layer switches \cite{al-fares_scalable_2008,bilal_quantitative_2013,bilal_taxonomy_2014}. Different layers may have different \gls{oversubscription} ratios \cite{bilal_quantitative_2013}. Usually high-end enterprise switches are used for core and aggregation layers, these switches are more expensive and consume more power than the commodity routers used for access layer \cite{bilal_quantitative_2013}. An example of a three-tier \gls{dcn} is shown in figure \ref{fig:bgnd:three_tier}.

\begin{figure}[!ht]
\centering
\subfigure{\includegraphics[width=\textwidth]{figures/three-tier.png}}
\caption{Three-tier \gls{data centre} architecture. Obtained from \cite{bilal_quantitative_2013}}
\label{fig:bgnd:three_tier}
\end{figure}

\paragraph{Fat-tree} \label{bgnd:dcn:fat-tree}
Fat-tree \cite{al-fares_scalable_2008} is a layered switch-centric architecture similar to three-tier. A $n$-ary fat-tree is composed by $n^2/4$ $n$-port core switches and $n$ pods. Each pod contains two layers (aggregation and edge) of $n/2$ $n$-port switches and $n^2/4$ servers. Each edge switch is connected to $n/2$ servers and to the $n/2$ aggregation switches in the pod. Each core switch is connected to one and only one aggregation switch in each pod.

The subnet of each aggregation and edge switch is given by the pattern $10.pod.switch.1$, where $pod$ and $switch$ represent the number of pod and switch respectively, with $pod \in [0,n-1]$, $switch \in [0,n/2-1]$ for edge switches and $switch \in [n/2-1,n-1]$ for aggregate switches. Servers subnet has the form $10.pod.switch.server$, where $pod$ and $switch$ are inherited from the edge switch to which the server is connected, and $server[2,n/2+1]$. Core switches can be seen as an array of dimension ($n/2 \times n/2$), and their subnet is defined by $10.n,i,j$, where $i,j \in [1,n/2]$. An example of a 4-ary fat-tree \gls{dcn} is shown in figure \ref{fig:bgnd:fat_tree}.

Fat-tree architecture is cheaper and more energy-efficient than three-tier, because it uses commodity switches for the three layers \cite{al-fares_scalable_2008}.

\begin{figure}[!ht]
\centering
\subfigure{\includegraphics[width=\textwidth]{figures/fat-tree.png}}
\caption{Fat-tree \gls{data centre} architecture with $n=4$. Obtained from \cite{bilal_quantitative_2013}}
\label{fig:bgnd:fat_tree}
\end{figure}

\paragraph{DCell} \label{bgnd:dcn:dcell}
DCell \cite{guo_dcell:_2008} is a recursive server-centric architecture. Starting in level-0, a $DCell_0$ is composed by $n$ servers connected to 1 mini-switch, this mini-switch provides communication between servers in the same $DCell_0$. Then a $DCell_1$ cell is composed by $n+1$ $DCell_0$ cells. Each server in a $DCell_0$ is connected to a server in another $DCell_0$ from the same $DCell_1$. The same procedure is applied recursively for further levels \cite{guo_dcell:_2008,bilal_quantitative_2013}. An example of a level-2 DCell \gls{dcn} is shown in figure \ref{fig:bgnd:dcell}.

\begin{figure}[!ht]
\centering
\subfigure{\includegraphics[width=\textwidth]{figures/dcell.png}}
\caption{Level-2 DCell \gls{data centre} architecture ($DCell_2$). Obtained from \cite{bilal_quantitative_2013}}
\label{fig:bgnd:dcell}
\end{figure}

\paragraph{BCube} \label{bgnd:dcn:bcube}
BCube \cite{guo_bcube:_2009} is another recursive server-centric architecture. Starting in level-0, a $BCube_0$ is composed by $n$ servers connected to a $n$-port mini-switch, which provides communication between servers in the same $BCube_0$. Then a $BCube_1$ is composed by $n$ $BCube_0$ and $n$ $n$-port switches, each server in a $BCube_0$ is connected to one of the $n$ level-1 $n$-port switches. The same procedure is applied recursively for further levels. 

In general, a $BCube_k$ is composed by $n$ $BCube_{k-1}$ and $n^k$ $n$-port switches, leading to a total of $n^{k+1}$ servers and $k+1$ levels of $n^k$ switches each. For any level $l \in \{0,...,k\}$, a $BCube_k$ contains $n^{k-l}$ $BCube_l$, each containing $n^{l+1}$ servers and $n^l$ switches. All the servers have $k+1$ ports, numbered from 0 to  $k$, where port $l$ is used to connect to a level-$l$ switch. In a $BCube_k$, the $k$-th port of the $i$-th server in the $j$-th $BCube_{k-1}$ is connected to the $j$-th port of the $i$-th switch \cite{guo_bcube:_2009}.

An example of a BCube \gls{dcn} is shown in figure \ref{fig:bgnd:bcube}.

\begin{figure}[!ht]
\centering
\subfigure{\includegraphics[width=0.9\textwidth]{figures/bcube.png}}
\caption{(a) Generic level-k BCube, composed by n $BCube_{k-1}$ and $n^k$ $n$-port switches. (b) Level-1 BCube with $n=4$. Both images obtained from \cite{guo_bcube:_2009}}
\label{fig:bgnd:bcube}
\end{figure}

\paragraph{FCell} \label{bgnd:dcn:fcell}
FCell \cite{li_fcell:_2015} is another recursive server-centric architecture, aimed to maintain low \gls{ntn delay} and satisfiable \gls{bisection bandwidth} while having a limited energy consumption. An $FCell(n)$ is composed by servers with 2 NIC ports and switches with $n$ ports. FCell is constructed by $n^2/2+1$ clusters, each one has $n^2/2$ servers, $n/2$ level-2 switches and $n$ level-1 switches. In each cluster, each level-2 switch is connected to all the level-1 switches, each level-1 switch is also connected to $n/2$ servers. Then each server of a cluster is directly connected to another server in another cluster. Figure \ref{fig:bgnd:bcube} shows the structure of a $FCell(4)$.

\begin{figure}[!ht]
\centering
\subfigure{\includegraphics[width=0.8\textwidth]{figures/fcell.png}}
\caption{$FCell(4)$ \gls{data centre} architecture. Obtained from \cite{li_fcell:_2015}}
\label{fig:bgnd:fcell}
\end{figure}

%\subsubsection{Comparison of architectures} \label{bgnd:dcn:arch:comparison}

%***********************************************************
%*****************  Energy Efficiency  *********************
%***********************************************************
\section{Energy-efficient routing algorithms} \label{bgnd:ee}
This section provides a brief review of recent energy-efficient routing algorithms for \glspl{dcn}. We classify the algorithms in two groups: greedy algorithms and heuristic optimisation algorithms.

\subsection{Greedy routing algorithms} \label{bgnd:greedy}
Energy-efficient routing algorithms are strongly based on \gls{soi} and \gls{woa} for unused network components. Some of these algorithms follow a greedy approach, starting with as few links as possible and only adding new links when it is absolutely necessary.

\subsubsection{Simple power-aware routing} \label{bgnd:greedy:simple}
One simple example is found in \cite{shang_network_2015}. The focus of this work is to compare the performance and energy efficiency of different \gls{dcn} architectures using two routing algorithms: the default routing algorithm for the architecture, and a proposed power-aware algorithm. The latter starts with a full network and tries to remove (put to sleep) as many components as possible, yet maintaining a minimum \gls{throughput} threshold level. The algorithm is summarised as follows:
    	    	
\begin{enumerate}
\item \underline{Initialisation:} initialise the full network, calculate total throughput.
\item \underline{First loop:} remove switches and re-route until throughput increment exceeds a given threshold. This procedure turns the whole switch to sleep.
\item \underline{Second loop:} remove links (ports) of active switches and re-route until throughput increment exceeds a given threshold. When no more switches can be put to sleep fully, the algorithm puts to sleep unnecessary ports.
\end{enumerate}

\subsubsection{Willow: energy consumption minimisation} \label{bgnd:greedy:willow}
In \cite{li_willow:_2015} the authors implemented an algorithm that works with a \gls{sdn} controller. The algorithm considers the effect of congestion into flow duration, and thus the total running time. The problem can be modelled as a \gls{mip}, whose objective is to minimise the aggregated working duration of the network ($\min \{ \sum{ t_s} \}$), subjected to the following constraints:

\begin{itemize}
    \item Each flow is assigned to only one path
    \item Switches are running if any of its links are in use
    \item Links are in use until all flows using the links are finished
    \item Flow deadline requirements
\end{itemize}

The algorithm schedules big flows of data to only one path and allocates small flows randomly to any path with enough residual capacity. The algorithm assigns flows to the path that minimizes the increment in the total network working duration, this increment is calculated with the \gls{sdn} controller.

The authors solved the \gls{mip} with \gls{cplex} and used the solution as a benchmark. They also compared the results with \gls{sa} and \gls{pso}, and Willow gave better results (closer to the \gls{mip} benchmark) and faster.

\subsubsection{Multi-resource green routing (MRG)} \label{bgnd:greedy:mrg}
In the \gls{naas} model, the network also executes processing of data and other services. Classical energy efficiency algorithms that put to sleep network components are not enough in this case because an optimal solution in that case may create a throughput bottleneck given by processing queues. In \cite{wang_multi-resource_2015} the authors propose an algorithm for energy-efficient flow scheduling and also implement a faster heuristic algorithm that exploits regularities of some architectures. However, this report will not address the latter because it is aimed to specific architectures.

\textbf{Problem definition:}
\begin{itemize}
\item \underline{Nodes:} the nodes are the servers ($v$), each server has a vector of resource capacities $C_v = [c_{v1},..., c_{vk}]$, where the index $k$ represents the different types of hardware resources of the server. The authors assume that all servers are equal and thus $C_{vk} = C_{wk} \equiv C_k, \forall v \in V, k \in K$. It is worth noting, that servers impose the capacity constraints and cost weights for the problem, and thus, they are located at the nodes instead of the edges of the graph. Therefore, the algorithm must transform node weights to edge weights previously, in order to use common shortest-path algorithms as subroutines.

\item \underline{Flows:} flows are described as a 3-tuple $d_m = (v_{ms}, v_{mt}, R_m)$, where $v_{ms}$ and $v_{mt}$ are the origin and destination nodes; and $R_m = [r_{m1},..., r_{mk}]$ is a vector of resources demands (for all types of demands). Flow resources requirements are constrained by node resource capacities $C_k$. For simplicity, the authors consider the resources as normalized by $C_k$, so $R_m \in \{0,1\}^K$.

\item \underline{Energy-Efficient Multi-Resource Routing (EEMR):} for each flow the algorithm must find a path in the network such that the aggregation of demands for any resource in a node does not exceed its capacity for that resource. The objective is to minimise the size of the set of nodes that are used to carry flows.
\end{itemize}

\textbf{Algorithm:} 
For each candidate flow $d_m$ do:
\begin{enumerate}
  \item Remove active nodes that cannot be used due to capacity constraints, generating sub-network $\mathcal{G_C}$.
  \item Check if the flow can be routed through $\mathcal{G_C}$.
  \item If none flow could be routed, pick one randomly and reset $\mathcal{G_C}$ as the whole set of nodes (both active and inactive) with enough capacity for the flow ($\mathcal{G_C}$).
  \item Calculate node weight assignment and transform to edge-weights.
  \item Find a shortest-path routing for the flow.
\end{enumerate}  

\textbf{Time complexity:} the time complexity of the algorithm is $O(|E|M^2)$, where $E$ is the set of edges and $M$ is the number of flow demands.

\textbf{Node weight assignment:} the idea behind this weight assignment is to distribute flows in a way that maintains an even use of node resources in the network. This is because by making use of resources evenly in a node, it is less likely that the node gets congested by the exhaustion of one resource.

In order to do this, the node weight is calculated based on the flow demand vector $R_m = [r_{m1},..., r_{mk}]$ and the node resource residual capacity vector $\tilde{C_v} = [\tilde{c}_{v1},..., \tilde{c}_{vk}]$, that represents the available resources. That is, $\tilde{C_v}$ contains the resource capacities minus the demands of all the flows already routed through node $v$, i.e. $\tilde{C_v} = [\tilde{c}_{v1},..., \tilde{c}_{vk}] = [1-r_{m1},..., 1-r_{mk}] = \{1\}^k - R_m$.

The weight $w(v)$ of a node $v$ with respect to a flow $m$ is calculated as the number of inversions between $\tilde{C_v}$ and $R_m$. Then the node weights are transformed to edge weights by simply averaging the weights of source and end nodes, that is, $w(v_1,v_2) = (w(v1) + w(v_2))/2$.

The number of inversions between two vectors $A = [a_1,...,a_n]$ and $B = [b_1,...,b_n]$ is the number of cases where $(a_i < a_j \land b_i > b_j) \lor (a_i > a_j \land b_i < b_j)$ with $i,j \in \{1,...,n\}$. The maximum number of inversions between any two vectors of dimension $n$ is $n(n-1)/2$. In order to make inactive nodes more ``expensive" than active ones, the node weight associated to inactive nodes is one unit larger than that maximum, which is: $w(v) = n(n-1)/2 + 1, \forall v \in V - V_a$.

%\subsubsection{Hierarchical green routing (HGR)} \label{bgnd:greedy:hgr}
%As stated in section \ref{bgnd:greedy:mrg}, in \cite{wang_multi-resource_2015}, the authors also implemented a topology-aware algorithm that exploits some properties of 

\subsection{Heuristic optimisation algorithms} \label{bgnd:heuristic}
In general terms, greedy approaches try to optimise a problem by taking minimum actions to improve (or degrade as low as possible) a current solution. However, network problems can be addressed by optimisation techniques, such as local search meta-heuristics. In this case a mathematical problem with an objective function is defined, and heuristics are applied to solve the problem.

One interesting example can be found in \cite{zahrani_genetic_2008}, where the authors solve the multicast problem using \glspl{ga} and \gls{lsa}. The multicast problem can be found in broadcasting applications, such as video conference streaming, e-learning, multimedia broadcasting, online television, among others \cite{zahrani_genetic_2008}. The algorithm is not specifically designed for energy efficient routing. However, since it minimises an objective function it is possible to extend it to energy-efficient multicast routing with a different objective function.

The problem consists in routing multiple broadcasts with one origin and multiple destinations. The authors implemented two algorithms: \gls{lsa} and \gls{gls} with PMX crossover. \Gls{lsa} is used in a pre-processing stage to estimate the maximum depth of local minima. Then in the \gls{gls}, each individual performs alternating sequences of descending and ascending steps.

\textbf{Problem definition:}
\begin{itemize}
\item \underline{Network:} mathematically the network is represented as a graph $G = (V,E)$, with weight and cost functions over the edges $C_o:E \rightarrow \mathbb{R}$ and $C_a:E \rightarrow \mathbb{R}$, respectively.

\item \underline{Flow requests:} the set of multicast requests have the form: $R_i = [v_{is} \rightarrow (v_{i1},...,v_{in}),C_i]$. Where, $v_{is}$ is the origin node, $D(R_i) = (v_{i1},...,v_{in})$ are the destination nodes, and $C_i$ is the minimum capacity requirement of the request $R_i$.

\item \underline{Problem:} a problem is defined by the network and the requests: $P =[G; C_o; C_a; R_1,...,R_n]$.

\begin{itemize}
   \item Objective function: minimise costs $Z(S)$ of configuration $S$, where $T(R)$ is the set of edges of tree associated with the request $R$ from configuration $S$. Defined by equations \ref{eqn:bgnd:steinerobj} and \ref{eqn:bgnd:steinerobjaux}.
      
   \item Capacity constraints on all edges, defined by equation \ref{eqn:bgnd:steinercap}
   
   \item \Gls{qos} constraints: various \gls{qos} constraints are defined (see section 2 in \cite{zahrani_genetic_2008} for further details). 
   
   \begin{gather}
   Z(S) = \sum_{R \text{ from } S} W(R)    \label{eqn:bgnd:steinerobj}\\
   W(R) = C \cdot \sum_{e \in T(R)} C_o(e) \label{eqn:bgnd:steinerobjaux}\\
   C_a(e) \ge C(R), \forall e \in T(R_i), \forall R_i \in S \label{eqn:bgnd:steinercap}
   \end{gather} 
\end{itemize}
\end{itemize}

\textbf{Algorithm:}
As stated previously, two algorithms were implemented: \gls{lsa} and \gls{gls}.

\begin{itemize}
   \item \underline{\gls{lsa} pre-processing:} \gls{lsa} is used in a pre-processing stage in order to estimate the maximum depth of local minima (symbolised with $\Gamma$)

   \item \underline{\gls{gls} algorithm:} Genetic algorithm with local search for each individual, implementing elitism and \gls{pmx}. Each individual performs quasi-deterministic local search in a downward direction. If it gets trapped in local minima they perform \gls{pmx} to get out. If after $L=50$ unsuccessful trials no better neighbour is found, the solution could be a potential local minimum, so individuals change from downsteps to upsteps. If individuals scape local minima (measured based on the $\Gamma$ estimation calculated in the pre-processing stage by \gls{lsa}) or after $L$ iterations, individuals change again to downsteps. Moreover, after $K$ steps the walk is interrupted by \gls{pmx} crossover. In each iteration the algorithm approximates the routing Steiner tree of a configuration $S$ with a KMB algorithm \cite{zahrani_genetic_2008}.
\end{itemize}

\subsection{Comparison of algorithms} \label{bgnd:ee:comparison}
Section \ref{bgnd:ee} reviewed different energy-efficient algorithms with different considerations and intended to slightly different things. The algorithm proposed by \cite{shang_network_2015} (see section \ref{bgnd:greedy:simple}) is intended to switch-centric \glspl{dcn}, and therefore it is not directly applicable to server-centric architectures.

Willow, proposed in \cite{li_willow:_2015} (see section \ref{bgnd:greedy:willow}), is oriented to both switch-centric and server-centric architectures. It also takes a step forward, by considering energy consumption instead of power consumption, and thus, addresses how congestion can increment duration of data flows and therefore the time during which certain network components are active. Nevertheless, this algorithm requires the interaction with a \gls{sdn} controller.

The MRG algorithm proposed in \cite{wang_multi-resource_2015} is oriented to server-centric \gls{naas} \glspl{dcn}. As explained in section \ref{bgnd:greedy:mrg}, the algorithm routes data flows with different resource requirements through a network of servers with determined capacities for each of these resources. Therefore, we believe this algorithm is the most general of the greedy algorithms reviewed. In deed, routing in server-centric \gls{dcn} with only one resource is just a particular case of this problem, where all the vectors of resource requirements and capacities have only one component. Moreover, the algorithm can be extended to switch-centric and hybrid architectures by defining appropriate capacities for the nodes that correspond to switches rather than servers.

Finally, the multicast routing algorithm proposed in \cite{zahrani_genetic_2008} (see section \ref{bgnd:heuristic}) is also extendible to other cases. In deed, it can also be adapted to energy efficiency by modifying the objective function in order to minimise energy or power consumption. However, extension to multi-resource routing would require further modifications to the problem definition, and probably to the algorithm itself.